<?php

return [
    'resources' => [
        'person' => [
            MiamiOH\RestngAddressService\Resources\AddressResourceProvider::class,
            MiamiOH\RestngAddressService\Resources\Addressv2ResourceProvider::class,
        ],
    ]
];