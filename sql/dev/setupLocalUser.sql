declare
  v_local_user_name varchar2(64) := null;
  v_local_user_pass varchar2(64) := null;

begin

  v_local_user_name := 'ADDRESS_WS_USER';
  v_local_user_pass := 'address';

  delete from ws_authentication_local_users where username = lower(v_local_user_name);

  insert into ws_authentication_local_users (username, algorithm, credential_hash, expiration_date)
    values (lower(v_local_user_name), 1, 
      lower(rawtohex(utl_raw.cast_to_raw(dbms_obfuscation_toolkit.md5(input_string=>v_local_user_pass)))),
      sysdate + 365);

  v_local_user_name := 'TEST_WS_USER';
  v_local_user_pass := 'test';

  delete from ws_authentication_local_users where username = lower(v_local_user_name);

  insert into ws_authentication_local_users (username, algorithm, credential_hash, expiration_date)
    values (lower(v_local_user_name), 1,
      lower(rawtohex(utl_raw.cast_to_raw(dbms_obfuscation_toolkit.md5(input_string=>v_local_user_pass)))),
      sysdate + 365);

 v_local_user_name := 'doej';
  v_local_user_pass := 'doej';

  delete from ws_authentication_local_users where username = lower(v_local_user_name);

  insert into ws_authentication_local_users (username, algorithm, credential_hash, expiration_date)
    values (lower(v_local_user_name), 1,
      lower(rawtohex(utl_raw.cast_to_raw(dbms_obfuscation_toolkit.md5(input_string=>v_local_user_pass)))),
      sysdate + 365);

  v_local_user_name := 'smithd';
  v_local_user_pass := 'smithd';

  delete from ws_authentication_local_users where username = lower(v_local_user_name);

  insert into ws_authentication_local_users (username, algorithm, credential_hash, expiration_date)
    values (lower(v_local_user_name), 1,
      lower(rawtohex(utl_raw.cast_to_raw(dbms_obfuscation_toolkit.md5(input_string=>v_local_user_pass)))),
      sysdate + 365);
end;
/