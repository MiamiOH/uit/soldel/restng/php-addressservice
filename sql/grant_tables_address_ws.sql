grant select on SATURN.SPBPERS to MUWS_GEN_RL;

grant select,insert,update,delete on SATURN.SPRADDR to MUWS_GEN_RL;

grant select on SATURN.STVNATN to MUWS_GEN_RL;

grant select on SATURN.STVSTAT to MUWS_GEN_RL;

grant execute on GB_ADDRESS to MUWS_GEN_RL;