<?php

namespace MiamiOH\RestngAddressService\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class AddressResourceProvider extends ResourceProvider
{
    public function registerDefinitions(): void
    {
        $this->addDefinition(array(

            'name' => 'Person.Address.PostResponse',
            'type' => 'object',
            'properties' => array(
                'pidm' => array(
                    'type' => 'number',
                ),
                'code' => array(
                    'type' => 'number',
                ),
                'message' => array(
                    'type' => 'string',
                ),
            ),
        ));

        $this->addDefinition(array(
            'name' => 'Person.Address.PostResponse.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Person.Address.PostResponse'
            )
        ));

        $this->addDefinition(array(
            'name' => 'Person.Address',
            'type' => 'object',
            'properties' => array(
                'pidm' => array(
                    'type' => 'number',
                ),
                'addressCode' => array(
                    'type' => 'number',
                ),
                'addressLine1' => array(
                    'type' => 'string',
                ),
                'addressLine2' => array(
                    'type' => 'string',
                ),
                'city' => array(
                    'type' => 'string',
                ),
                'state' => array(
                    'type' => 'string',
                ),
                'zip' => array(
                    'type' => 'string',
                ),
                'sequenceNumber' => array(
                    'type' => 'string',
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'Person.Address.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Person.Address'
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(

            'name' => 'Address',
            'class' => 'MiamiOH\RestngAddressService\Services\Address',
            'description' => 'Information about address',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'configuration' => array('type' => 'service', 'name' => 'APIConfiguration'),
                'dataSource' => array('type' => 'service', 'name' => 'APIDataSourceFactory'),

            ),
        ));
    }

    public function registerResources(): void
    {
        $this->addResource(
            array(

                'action' => 'create',
                'name' => 'person.address.post',
                'description' => 'Create new address number with pidm',
                'pattern' => '/person/address/v1',
                'service' => 'Address',
                'method' => 'createAddress',
                'tags' => array('Person'),
                //'method' => 'postAddress',
                'middleware' => array(
                    'authenticate' => array('type' => 'token'),
                ),
                'returnType' => 'collection',
                'body' => array(
                    'description' => 'An address object',
                    'required' => true,
                    'schema' => array(
                        '$ref' => '#/definitions/Person.Address'
                    )
                ),
                'responses' => array(
                    App::API_CREATED => array(
                        'description' => 'Results of creation of address records',
                        'returns' => array(
                            'type' => 'model',
                            '$ref' => '#/definitions/Person.Address.PostResponse.Collection',

                            /*App::API_UNAUTHORIZED => array(
                                'description' => 'Unauthorized Access',
                                */
                        )
                    ),
                )
            )
        );

        $this->addResource(
            array(

                'action' => 'read',
                'name' => 'person.address.get',
                'description' => "Read a person's address",
                'pattern' => '/person/address/v1',
                'service' => 'Address',
                'method' => 'getAddress',
                'isPageable' => false,
                'tags' => array('Person'),
                'returnType' => 'collection',
                'options' => array(
                    'pidm' => array('type' => 'list', 'description' => 'pidm(s) to get address records'),
                ),
                'middleware' => array(
                    'authenticate' => array('type' => 'token'),
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'A collection of address records',
                        'returns' => array(
                            'type' => 'array',
                            '$ref' => '#/definitions/Person.Address.Collection',
                        )
                    ),
                )
            )
        );

        $this->addResource(array(

            'action' => 'update',

            'name' => 'person.address.put',

            'description' => 'Update existing address numbers',

            'pattern' => '/person/address/v1',

            'service' => 'Address',

            'tags' => array('Person'),

            'method' => 'putAddress',

            'middleware' => array(
                'authenticate' => array('type' => 'token'),
            ),

            'returnType' => 'collection',
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A collection of Addresses',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/Person.Address.Collection',
                    )
                )

            )));
    }

    public function registerOrmConnections(): void
    {
    }
}
