<?php

namespace MiamiOH\RestngAddressService\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class Addressv2ResourceProvider extends ResourceProvider
{
    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'Person.Address.v2',
            'type' => 'object',
            'properties' => array(
                'pidm' => array(
                    'type' => 'string',
                ),
                'addressType' => array(
                    'type' => 'string',
                ),
                'streetLine1' => array(
                    'type' => 'string',
                ),
                'streetLine2' => array(
                    'type' => 'string',
                ),
                'streetLine3' => array(
                    'type' => 'string',
                ),
                'city' => array(
                    'type' => 'string',
                ),
                'state' => array(
                    'type' => 'string',
                ),
                'postalCode' => array(
                    'type' => 'string',
                ),
                'nationBannerCode' => array(
                    'type' => 'string',
                ),
                'nationDescription' => array(
                    'type' => 'string',
                ),
                'sequenceNumber' => array(
                    'type' => 'number',
                ),
                'fromDate' => array(
                    'type' => 'string',
                ),
                'toDate' => array(
                    'type' => 'string',
                ),
                'status' => array(
                    'type' => 'string',
                ),
                'id' => array(
                    'type' => 'string'
                ),
                'addressSourceCode' => array(
                    'type' => 'string'
                )
            )
        ));

        $this->addDefinition(array(
            'name' => 'Person.Address.v2.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Person.Address.v2'
            )
        ));

        $this->addDefinition(array(

            'name' => 'Person.Address.v2.PostResponse',
            'type' => 'object',
            'properties' => array(
                'status' => array(
                    'type' => 'number',
                ),
                'message' => array(
                    'type' => 'string',
                ),
                'address' => array(
                    'type' => 'object',
                ),
                'errorCode' => array(
                    'type' => 'string',
                ),
                'errorString' => array(
                    'type' => 'string',
                ),
                'suggestionList' => array(
                    'type' => 'string',
                ),
            ),
        ));

        $this->addDefinition(array(
            'name' => 'Person.Address.v2.PostResponse.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Person.Address.v2.PostResponse'
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'Addressv2',
            'class' => 'MiamiOH\RestngAddressService\Services\Addressv2',
            'description' => 'Provide Address information for the given unique ID.',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'updateAddressv2' => array('type' => 'service', 'name' => 'UpdateAddressv2'),
                'cleanAddress' => array('type' => 'service', 'name' => 'CleanAddress'),
            ),
        ));

        $this->addService(array(
            'name' => 'Addressv2REST',
            'class' => 'MiamiOH\RestngAddressService\Services\Addressv2REST',
            'description' => 'Provide Address information for the given unique ID.',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'bannerUtil' => array('type' => 'service', 'name' => 'MU\BannerUtil'),
                'addressv2' => array('type' => 'service', 'name' => 'Addressv2'),
            ),
        ));

        $this->addService(array(
            'name' => 'UpdateAddressv2',
            'class' => 'MiamiOH\RestngAddressService\Services\UpdateAddressv2',
            'description' => 'Provide Update Address Service.',
        ));

        $this->addService(array(
            'name' => 'CleanAddress',
            'class' => 'MiamiOH\RestngAddressService\Services\CleanAddress',
            'description' => 'Provide Clean Address Service.',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
            ),
        ));
    }

    public function registerResources(): void
    {
        $this->addResource(
            array(

                'action' => 'read',
                'name' => 'person.address.v2.muid.get',
                'description' => "Read a person's address",
                'pattern' => '/person/address/v2/:muid',
                'service' => 'Addressv2REST',
                'method' => 'getAddressv2',
                'tags' => array('Person'),
                'returnType' => 'model',
                'params' => array(
                    'muid' => array('description' => 'A Miami identifier', 'alternateKeys' => ['uniqueId', 'pidm']),
                ),
                'options' => array(
                    'addressType' => array(
                        'enum' => ['LO', 'RH', 'MA'],
                        'description' => 'Display Address records based on addresstype ',
                        'type' => 'list'),

                    'status' => array(
                        'enum' => ['active', 'inactive', 'future'],
                        'description' => 'Display Address records based on status indicator ',
                        'type' => 'list'),
                ),
                'middleware' => array(
                    'authenticate' => array(
                        array(
                            'type' => 'token'
                        )
                    ),
                    'authorize' => array(
                        array(
                            'application' => 'WebServices',
                            'module' => 'Person-Address',
                            'key' => 'view',
                        ),
                    ),
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'A collection of address records',
                        'returns' => array(
                            'type' => 'array',
                            '$ref' => '#/definitions/Person.Address.v2.Collection',
                        )
                    ),
                )
            )
        );

        $this->addResource(
            array(

                'action' => 'create',
                'name' => 'person.address.v2.muid.create',
                'description' => 'Create new address record with muid(uniqueId,pidm)',
                'pattern' => '/person/address/v2/:muid',
                'service' => 'Addressv2REST',
                'method' => 'postAddressv2Collection',
                'tags' => array('Person'),
                'params' => array(
                    'muid' => array('description' => 'A Miami identifier', 'alternateKeys' => ['uniqueId', 'pidm']),
                ),
                'options' => array(
                    'skipValidation' => array(
                        'enum' => array('yes', 'no'),
                        'default' => 'no',
                        'description' => 'Skip the validation by clean address '
                    ),
                ),
                'middleware' => array(
                    'authenticate' => array(
                        array(
                            'type' => 'token'
                        )
                    ),
                    'authorize' => array(
                        array(
                            'application' => 'WebServices',
                            'module' => 'Person-Address',
                            'key' => 'create',
                        ),
                    ),
                ),
                'body' => array(
                    'description' => 'An address object',
                    'required' => true,
                    'schema' => array(
                        '$ref' => '#/definitions/Person.Address.v2.Collection'
                    )
                ),
                'responses' => array(
                    App::API_CREATED => array(
                        'description' => 'Results of creation of address records',
                        'returns' => array(
                            'type' => 'model',
                            '$ref' => '#/definitions/Person.Address.v2.PostResponse.Collection',

                        )
                    ),
                )
            )
        );

        $this->addResource(array(
            'action' => 'delete',
            'name' => 'person.contact.v1.id.delete',
            'description' => 'Delete an Address record',
            'tags' => array('Person'),
            'pattern' => '/person/address/v2/:id',
            'service' => 'Addressv2REST',
            'method' => 'deleteAddressv2',
            'params' => array(
                'id' => array('description' => 'Address Id'),
            ),
            'middleware' => array(
                'authenticate' => array(
                    array(
                        'type' => 'token'
                    )
                ),
                'authorize' => array(
                    array(
                        'application' => 'WebServices',
                        'module' => 'Person-Address',
                        'key' => 'delete',
                    ),
                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'No body content returned'
                ),
            )
        ));

        $this->addResource(
            array(

                'action' => 'update',
                'name' => 'person.address.v2.muid.update',
                'description' => "Update a person's address",
                'pattern' => '/person/address/v2/:muid',
                'service' => 'Addressv2REST',
                'method' => 'updateAddressv2',
                'isPageable' => false,
                'tags' => array('Person'),
                'returnType' => 'collection',
                'params' => array(
                    'muid' => array('description' => 'A Miami identifier', 'alternateKeys' => ['uniqueId', 'pidm']),
                ),
                'options' => array(
                    'addressType' => array(
                        'enum' => ['LO', 'RH'],
                        'description' => 'Address records to update based on addresstype '),
                ),
                'middleware' => array(
                    'authenticate' => array(
                        array(
                            'type' => 'token'
                        )
                    ),
                    'authorize' => array(
                        array(
                            'application' => 'WebServices',
                            'module' => 'Person-Address',
                            'key' => 'update',
                        ),
                    ),
                ),
                'body' => array(
                    'description' => 'An address to be updated',
                    'required' => true,
                    'schema' => array(
                        '$ref' => '#/definitions/Person.Address.v2.Collection'
                    )
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'Successfully updated the status',
                    ),
                )
            )
        );

        $this->addResource(
            array(

                'action' => 'read',
                'name' => 'person.address.v2.get',
                'description' => "Read a person's address",
                'pattern' => '/person/address/v2',
                'service' => 'Addressv2REST',
                'method' => 'getAddressV2Collection',
                'isPageable' => false,
                'tags' => array('Person'),
                'returnType' => 'collection',
                'options' => array(
                    'pidm' => array('required' => true, 'type' => 'list', 'description' => 'pidm(s) to get address records'),
                    'addressType' => array(
                        'enum' => ['LO', 'RH', 'MA'],
                        'description' => 'Address records to return based on addresstype ',
                        'type' => 'list'),

                    'status' => array(
                        'enum' => ['active', 'inactive'],
                        'description' => 'Address records to return based on status indicator '),
                ),
                'middleware' => array(
                    'authenticate' => array('type' => 'token'),
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'A collection of address records',
                        'returns' => array(
                            'type' => 'array',
                            '$ref' => '#/definitions/Person.Address.v2.Collection',
                        )
                    ),
                )
            )
        );
    }

    public function registerOrmConnections(): void
    {
    }
}
