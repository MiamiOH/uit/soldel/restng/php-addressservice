<?php

/*
-----------------------------------------------------------
FILE NAME: address.class.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Jimmy Xu

DESCRIPTION:  The address service is designed for get only (other functions can be added later)


Parameter where in get: This is a where clause provided to developer for debuging purpose. It should not be used
by consumer app. The service currently returns all information about a person and more parameters can be added in
the future

Protected group: SPBPERS_CONFID_IND = 'Y'. We will not update/get/insert any data for this people. Error message will
not be sent out but the consumer app should know the feature of this service.

INPUT:
PARAMETERS: pidm, sequence number and uniqueId

ENVIRONMENT DEPENDENCIES: RESTNG FRAMEWORK

TABLE USAGE:
    SATURN.SPRADDR (SELECT)
    SATURN.SPBPERS (SELECT)

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

07/21/2015       XUJ4
Description:  Initial Program

09/01/2015		 SCHMIDEE
Description: Modify to use correct data source.

09/29/2015		 SCHMIDEE
Description: Corrections for SQL Injection Vunlerablities and Security Checks. Added
             Comments and cleaned up code.

10/12/2015		 SCHMIDEE
Description: Added POST Method.

11/24/2015       PATELAH
Description: Made necessary changes to reflect messages instead of success/failure counts.

11/30/2015       PATELAH
Description: Code clean up.

03/24/2015       SCHMIDEE
Description: Removed Lowercasing on Addresses returned from GET method. Removed the
             exclusion of confidential address and address who's SPRADDR_TO_DATE is in
             the future from GET return due to requirements needed by the debt projection
             project.


 */

namespace MiamiOH\RestngAddressService\Services;

class Address extends \MiamiOH\RESTng\Service
{
    private $dataSource = '';
    private $configuration = '';
    private $datasource_name = 'MUWS_SEC_PROD'; // secure datasource

    /*
    * Helper function to Set the Data Source to be used called by the frame work
    *
    * Inputs:
    * datasource: Name of Data Source to use.
    */
    public function setDataSource($datasorce)
    {
        $this->dataSource = $datasorce;
    }

    /*
    * Helper function to Set the Database source to be Used
    *
    * Inputs:
    * database: Name of database source to use.
    */
    public function setDatabase($database)
    {
        $this->database = $database;
    }

    /*
    * Helper function to Set the Configuration to be Used
    *
    * Inputs:
    * configuration: Name of Configuration source to use.
    */
    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    /*
     * Updates Address Information (PUT)
     *
     * Parameters (URL):
     * token: Authentication Token (WebServices/Person update or All Access Required)
     *
     * Not Implemented yet
     */
    public function putAddress()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $payload = $request->getData();

        //Confirm User Can actually Access this Information
        $user = $this->getApiUser();

        $authorized = $user->isAuthorized('WebServices', 'Person', 'All');
        if (!$authorized) {
            $authorized = $user->isAuthorized('WebServices', 'Person', 'update');
        }

        if (!$authorized) {
            $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED); //401
            return $response;
        }
        //User Is Authorized

        if ($payload) {//if consumer provides data, perform the database transactions
            //$this->updateAddressData($payload);
        }

        $response->setPayload($payload);//todo: create a return report
        $response->setStatus(\MiamiOH\RESTng\App::API_OK); //200
        return $response;
    }

    /*
     * Updates Address Information (POST)
     *
     * Parameters (URL):
     * token: Authentication Token (WebServices/Person update or All Access Required)
     *
     * Not Implemented yet
     */
    public function postAddress()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $payload = $request->getData();
        $result = null;

        //Confirm User Can actually Access this Information
        $user = $this->getApiUser();

        $authorized = $user->isAuthorized('WebServices', 'Person', 'All');
        if (!$authorized) {
            $authorized = $user->isAuthorized('WebServices', 'Person', 'update');
        }

        if (!$authorized) {
            $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
            return $response;
        }
        //User is Authorized

        if ($payload) {//if consumer provides data, perform the database transactions
            $result = $this->createAddress($payload, $user->getUsername());
        }

        $response->setPayload($result);
        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        return $response;
    }

    public function createAddress($payload, $username)
    {
        $successPidms = array();
        $failedPidms = array();

        //$successCount = 0;
        //$failedCount  = 0;

        $report = array();
        $errorReport = "";
        $results = false;

        $today = date("d-M-y");

        foreach ($payload as $addressrecord) {
            $protected = 0;

            if ((isset($addressrecord['pidm']) && $addressrecord['pidm'] != '') &&
                (isset($addressrecord['addressType']) && $addressrecord['addressType'] != '')
                && (isset($addressrecord['city']) && $addressrecord['city'] != '')
                && (isset($addressrecord['userId']) && $addressrecord['userId'] != '')
                && (isset($addressrecord['dataOrigin']) && $addressrecord['dataOrigin'] != '')) {
                $pidm = trim($addressrecord['pidm']);
                $address_type = $addressrecord['addressType'];
                $street_line1 = isset($addressrecord['streetLine1']) ? $addressrecord['streetLine1'] : '';
                $street_line2 = isset($addressrecord['streetLine2']) ? $addressrecord['streetLine2'] : '';
                $street_line3 = isset($addressrecord['streetLine3']) ? $addressrecord['streetLine3'] : '';
                $street_line4 = isset($addressrecord['streetLine4']) ? $addressrecord['streetLine4'] : '';
                $to_date = isset($addressrecord['toDate']) ? $addressrecord['toDate'] : '';
                $country_code = isset($addressrecord['countryCode']) ? $addressrecord['countryCode'] : '';
                $nation_code = isset($addressrecord['nationCode']) ? $addressrecord['nationCode'] : '';
                $phone_area = isset($addressrecord['phoneArea']) ? $addressrecord['phoneArea'] : '';
                $phone_number = isset($addressrecord['phoneNumber']) ? $addressrecord['phoneNumber'] : '';
                $phone_extension = isset($addressrecord['phoneExtension']) ? $addressrecord['phoneExtension'] : '';
                $status_indicator = isset($addressrecord['statusIndicator']) ? $addressrecord['statusIndicator'] : '';
                $asrc_code = isset($addressrecord['asrcCode']) ? $addressrecord['asrcCode'] : '';
                $delivery_point = isset($addressrecord['deliveryPoint']) ? $addressrecord['deliveryPoint'] : '';
                $correction_digit = isset($addressrecord['correctionDigit']) ? $addressrecord['correctionDigit'] : '';
                $carrier_route = isset($addressrecord['carrierRoute']) ? $addressrecord['carrierRoute'] : '';
                $gst_tax_id = isset($addressrecord['gstTaxId']) ? $addressrecord['gstTaxId'] : '';
                $reviewed_indicator = isset($addressrecord['reviewedIndicator']) ? $addressrecord['reviewedIndicator'] : '';
                $reviewed_user = isset($addressrecord['reviewedUser']) ? $addressrecord['reviewedUser'] : '';
                $house_number = isset($addressrecord['houseNumber']) ? $addressrecord['houseNumber'] : '';
                $surrogate_id = isset($addressrecord['surrogateId']) ? $addressrecord['surrogateId'] : '';
                $vpdi_code = isset($addressrecord['vpdiCode']) ? $addressrecord['vpdiCode'] : '';
                $version = isset($addressrecord['version']) ? $addressrecord['version'] : '';
                $city = $addressrecord['city'];
                $state_code = isset($addressrecord['stateCode']) ? $addressrecord['stateCode'] : '';
                $zip = isset($addressrecord['zip']) ? $addressrecord['zip'] : 'Y';
                $data_orgin = $addressrecord['dataOrigin'];
                $user_id = $addressrecord['userId'];
                $duplicated = 0;
                $errorReport = $pidm . " has Unknown error, check log";
                $results = false;
                try {
                    //Check Confidentiality Flag and for if Record Already Exists
                    $dbh = $this->database->getHandle($this->datasource_name);

                    $getCheck = "select spraddr_pidm, 
									   	spraddr_atyp_code, 
									    spraddr_seqno, 
									    spraddr_street_line1, 
									    spraddr_street_line2,
									    spraddr_street_line3,
									    spraddr_street_line4,
									    spraddr_city,
									    spraddr_stat_code,
									    spraddr_zip,
									    spbpers_confid_ind
								 from spraddr, spbpers
								 where spbpers_pidm = spraddr_pidm
								 and spraddr_pidm = ?
								 order by spraddr_seqno desc";

                    $duplicateCheckResults = $dbh->queryall_array($getCheck, $pidm);
                    $address_seqno = 1;
                    if ($duplicateCheckResults) {
                        foreach ($duplicateCheckResults as $address) {
                            if ($address_seqno < intval($address['spraddr_seqno'])) {
                                $address_seqno = intval($address['spraddr_seqno']);
                            }

                            if ($address['spbpers_confid_ind'] == 'Y') {
                                $protected = 1;
                                break;
                            }

                            if ($address_type == $address['spraddr_atyp_code']
                                && $street_line1 == $address['spraddr_street_line1']
                                && $street_line2 == $address['spraddr_street_line2']
                                && $street_line3 == $address['spraddr_street_line3']
                                && $street_line4 == $address['spraddr_street_line4']
                                && $city == $address['spraddr_city']
                                && $state_code == $address['spraddr_stat_code']
                                && $zip == $address['spraddr_zip']) {
                                $duplicated = 1;
                                break;
                            }
                        }
                    }

                    if ($duplicated == 0) {
                        if ($addressrecord['sequenceNumber']) {
                            $address_seqno = $addressrecord['sequenceNumber'];
                        } else {
                            $address_seqno = $address_seqno + 1;
                        }

                        if ($protected == 0) {
                            $results = $dbh->perform(
                                'insert into spraddr (spraddr_pidm, spraddr_atyp_code, spraddr_seqno, spraddr_from_date, spraddr_to_date,
																		   spraddr_street_line1, spraddr_street_line2, spraddr_street_line3, spraddr_city, spraddr_stat_code,
																		   spraddr_zip, spraddr_cnty_code, spraddr_natn_code, spraddr_phone_area, spraddr_phone_number,
																		   spraddr_phone_ext, spraddr_status_ind, spraddr_activity_date, spraddr_user, spraddr_asrc_code,
																		   spraddr_delivery_point, spraddr_correction_digit, spraddr_carrier_route, spraddr_gst_tax_id, spraddr_reviewed_ind,
																		   spraddr_reviewed_user, spraddr_data_origin, spraddr_ctry_code_phone, spraddr_house_number, spraddr_street_line4,
																		   spraddr_surrogate_id, spraddr_version, spraddr_user_id, spraddr_vpdi_code) 
																		   values(?, ?, ?, sysdate, ?,
																				 ?, ?, ?, ?, ?,
																				 ?, ?, ?, ?, ?,
																				 ?, ?, sysdate, ?, ?,
																				 ?, ?, ?, ?, ?,
																				 ?, ?, ?, ?, ?,
																				 ?, ?, ?, ?)',
                                array($pidm, $address_type, $address_seqno, $to_date,
                                    $street_line1, $street_line2, $street_line3, $city, $state_code,
                                    $zip, $country_code, $nation_code, $phone_area, $phone_number,
                                    $phone_extension, $status_indicator, $user_id, $asrc_code,
                                    $delivery_point, $correction_digit, $carrier_route, $gst_tax_id, $reviewed_indicator,
                                    $reviewed_user, $user_id, $country_code, $house_number, $street_line4,
                                    $surrogate_id, $version, $user_id, $vpdi_code)
                            );
                        } else {
                            $errorReport = 'Cannot be updated/Inserted, data protected';
                        }
                    } else {
                        $errorReport = 'Duplicate entry, check address ' . $street_line1 . ", " .
                            $street_line2 . ", " . $street_line3 . ", " . $city . ", " . $state_code . " " . $zip . ' type ' . $address_type . '';
                    }
                } catch (\Exception $e) {
                    $errorReport = $e->getMessage();
                    if (strpos($errorReport, 'unique constraint') !== false) {
                        $errorReport = 'Unique Constraint violation, check pidm and sequence number combination';
                    }
                    throw new \Exception('Error inserting data: ' . $e);
                }
                if ($results == true) {
                    $report[] = array('pidm' => $pidm, 'code' => \MiamiOH\RESTng\App::API_CREATED, 'message' => '');
                //$successCount += 1;
                    //array_push($successPidms, $pidm);
                } else {
                    $report[] = array('pidm' => $pidm, 'code' => \MiamiOH\RESTng\App::API_FAILED, 'message' => $errorReport);
                    //$failedCount += 1;
                    //array_push($failedPidms, $errorReport);
                }
            } else {
                $report[] = array('pidm' => (isset($addressrecord['pidm']) ? $addressrecord['pidm'] : ''), 'code' => \MiamiOH\RESTng\App::API_FAILED, 'message' => "Missing Required Field.");
                //$failedCount += 1;
                //array_push($failedPidms, (isset($addressrecord['pidm'])? $addressrecord['pidm'] : "").": Missing Required Field. Make sure that PIDM, City, and Address Type are Set.");
            }
        }

        return $report;
    }

    /*
     * Get Address Information (GET)
     *
     * Parameters (URL):
     * token: Authentication Token (WebServices/Person view or All Access Required)
     * pidms: PIDM's to Retrived
     * uniqueid: UniqueID's to Retrive (Removed to not being used)
     *
     * Return: Returns an array of all address of the users requested unless they have the
     *		  confidential flag.
     */
    public function getAddress()
    {
        //log
        $this->log->debug('Start the address service.');

        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();
        $payload = array();
        //$additionalwhere = null;
        $pidms = array();

        //Confirm User Can actually Access this Information
        $user = $this->getApiUser();

        $authorized = $user->isAuthorized('WebServices', 'Person', 'All');
        if (!$authorized) {
            $authorized = $user->isAuthorized('WebServices', 'Person', 'view');
        }

        if (!$authorized) {
            $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);//401
            return $response;
        }

        //User is Authorized to Retrive Information
        if ((isset($options['pidm']) && is_array($options['pidm']))
            || (isset($options['uniqueid']) && is_array($options['uniqueid']))) {

            // Gather All PIDMS that need to be Retrived

            if (is_array($options['pidm'])) {
                $pidms = $options['pidm'];
            }

            //Get Addresses and build Payload to Return
            $payload = $this->buildPayload($this->getAddressData($pidms));
        } elseif ($request->getResourceParam('pidm')) {
            $termcrn_arr = explode(',', $request->getResourceParam('pidm'));
        } else {
            throw new \Exception('Error getting options or parameter: No pidm is available');
        }

        //Response was successful and Return information
        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);
        return $response;
    }

    /*
     * Helper Method to Get Address Information for PIDMs
     *
     * Parameters:
     * pidms: PIDM's to Retrived
     *
     * Return: Returns an array of all address of the PIDMs requested unless they have the
     *		  confidential flag.
     */
    private function getAddressData($pidms)
    {
        $pidmString = '';
        try {
            //parse the input pidm list
            if ($pidms) {
                $values = [];
                $placeHolders = [];
                foreach ($pidms as $pidm) {
                    $values[] = $pidm;
                    $placeHolders[] = '?';
                }
                $placeHolderString = implode(', ', $placeHolders);
                $pidmString = substr($pidmString, 0, -1);

                $dbh = $this->database->getHandle($this->datasource_name);
                $queryString = "SELECT spraddr_pidm,
								spraddr_atyp_code,
								spraddr_street_line1,
								spraddr_street_line2,
								spraddr_city,
								spraddr_stat_code,
								spraddr_zip,
								spraddr_seqno
								FROM spraddr,SPBPERS where spraddr_pidm in ($placeHolderString) 
								and SPBPERS_PIDM = SPRADDR_PIDM
								and (SPRADDR_TO_DATE is NULL or SPRADDR_TO_DATE > SYSDATE)
								order by SPRADDR_pidm, spraddr_seqno desc";
                $results = $dbh->queryall_array($queryString, $values);
            } else {
                throw new \Exception('Pidm/UniqueId is required to get address numbers');
            }
        } catch (\Exception $e) {
            throw new \Exception('Error getting data: ' . $e);
        }

        return $results;
    }

    /*
     * Helper to build return payload.
     *
     * Parameters:
     * addressdata: Associtive array of address data to process.
     *
     * Return: Associtive Array of data that should be returned.
     */
    private function buildPayload($addressdata)
    {
        $records = array();
        $i = 0;
        foreach ($addressdata as $oneAddress) {
            $internal = array(
                'pidm' => $oneAddress['spraddr_pidm'],
                'addressCode' => $oneAddress['spraddr_atyp_code'],
                'addressLine1' => $oneAddress['spraddr_street_line1'],
                'addressLine2' => $oneAddress['spraddr_street_line2'],
                'city' => $oneAddress['spraddr_city'],
                'state' => $oneAddress['spraddr_stat_code'],
                'zip' => $oneAddress['spraddr_zip'],
                'nationBannerCode' => $oneAddress['spraddr_natn_code'],
                'nationDescription' => $oneAddress['stvnatn_nation'],
                'sequenceNumber' => $oneAddress['spraddr_seqno'],

            );
            $records['address' . $i] = $internal;
            $i++;
        }

        return $records;
    }
}
