<?php

namespace MiamiOH\RestngAddressService\Services;

use Carbon\Carbon;
use MiamiOH\RestngAddressService\Exceptions\AddressExistsException;

class Addressv2 extends \MiamiOH\RESTng\Service
{
    public const DATE_FORMAT = 'Y-m-d';
    private $dataSourceName = 'MUWS_GEN_PROD';

    private $dbh;

    private $filterTypeList = [];
    private $filterStatusList = [];
    private $filterSkipValidation = '';


    /** @var \MiamiOH\RestngAddressService\Services\UpdateAddressv2 $updateAddress */
    private $updateAddress;

    /** @var  \MiamiOH\RestngAddressService\Services\CleanAddress $cleanAddress */
    private $cleanAddress;

    /**
     * @param $updateAddress
     */
    public function setUpdateAddressv2($updateAddress)
    {
        /** @var \MiamiOH\RestngAddressService\Services\UpdateAddressv2 $updateAddress */
        $this->updateAddress = $updateAddress;
    }

    /**
     * @param $cleanAddress
     */
    public function SetCleanAddress($cleanAddress)
    {
        $this->cleanAddress = $cleanAddress;
    }

    /**
     * @param $database
     */
    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->dataSourceName);
    }

    /**
     * This methos called from REST to get the clean Address response
     * @return mixed
     */
    public function getCleanAddressResponse()
    {
        return $this->cleanAddress->getCleanAddressResponse();
    }

    /*
    * Helper Method to Get Address Information for PIDMs
    *
    * Parameters:
    * pidms: PIDMs of record to be retrieved
    *
    * Return: Returns collection of all address of the PIDMs requested
    */
    public function read($pidms)
    {
        if (is_scalar($pidms)) {
            $pidms = array($pidms);
        }
        $values = $pidms;

        $queryString = "SELECT spraddr_pidm,
                               spraddr_atyp_code,
                               spraddr_status_ind,
                               to_char(spraddr_from_date,'YYYY-MM-DD') as spraddr_from_date,
                               to_char(spraddr_to_date, 'YYYY-MM-DD') as spraddr_to_date,
                               spraddr_street_line1,
                               spraddr_street_line2,
                               spraddr_street_line3,
                               spraddr_city,
                               spraddr_stat_code,
                               spraddr_zip,
                               spraddr_natn_code,
                               stvnatn_nation,
                               spraddr_seqno,
                               spraddr_asrc_code,
                               ROWIDTONCHAR(spraddr.ROWID) as row_id
                               FROM spraddr left join stvnatn on stvnatn_code = spraddr_natn_code 
                               WHERE spraddr_pidm IN (" . implode(',', array_fill(0, count($pidms), '?')) . ")";

        if (count($this->filterTypeList)) {
            $placeHolders = [];
            foreach ($this->filterTypeList as $type) {
                $values[] = $type;
                $placeHolders[] = '?';
            }
            $queryString .= '
                    AND spraddr_atyp_code in (' . join(', ', $placeHolders) . ')';
        }

        if (count($this->filterStatusList)) {
            $queryString .= 'and (';
            $count = 0;
            foreach ($this->filterStatusList as $status) {
                // $count > 0 indicates that filterStatusList is having more than one value
                // for example filerStatusList=['active','future']. Then we need to 'or' queries
                // for 'active' and 'future'.
                if ($count > 0) {
                    $queryString .= ' or ';
                }

                switch ($status) {
                        // All the active records with from date is in past and to date is in future
                    case 'active':
                        $queryString .= 'spraddr_status_ind is null
                            and (spraddr_from_date is null
                            or spraddr_from_date <= sysdate)
                            and (spraddr_to_date is null
                            or spraddr_to_date > sysdate)';
                        break;
                        // All the records with status as inactive
                    case 'inactive':
                        $queryString .= '
                        spraddr_status_ind = \'I\'';
                        break;
                        // All the active records with from date is in future
                    case 'future':
                        $queryString .= 'spraddr_status_ind is null
                            and spraddr_from_date > sysdate';
                }
                $count++;
            }
            $queryString .= ')';
        }

        $queryString .= '
                ORDER BY spraddr_seqno
            ';

        $records = $this->dbh->queryall_array($queryString, $values);

        for ($i = 0; $i < count($records); $i++) {
            $records[$i] = $this->makeModelFromRecord($records[$i]);
        }

        $this->clearFilters();

        return $records;
    }

    /*
     * This method creates new record.
     * If the addressType is 'LO' then calls endLocalAddress() to end the previous active LO record
     * and creates new LO record.
     * TODO:Adding endPrevious option provides flexibility to choose if previous active record needs to be ended or not
     */

    public function create($address)
    {
        $cleanAddressResponse = [];
        if (
            isset($address['pidm']) && ($address['pidm'] != '') &&
            isset($address['addressType']) && ($address['addressType'] != '') &&
            isset($address['streetLine1']) && ($address['streetLine1'] != '') &&
            isset($address['city']) && ($address['city'] != '') &&
            isset($address['state']) && ($address['state'] != '') &&
            isset($address['postalCode']) && ($address['postalCode'] != '')
        ) {
            try {
                /* Check whether the user has chosen the Skip Validation option. Clean address gets called only if skip validation = 'no' */

                if ($this->filterSkipValidation == 'no') {
                    /* Call CleanAddress method to verify the address, if address is valid
                    it returns the $address with minimal correction. For example Court is replaced with Ct. */

                    /* Clean Address verification is only for LO address type.
                       Call CleanAddress method to verify the address, if address is valid
                       it returns the $address with minimal correction. For example Court is replaced with Ct. */
                    if ($address['addressType'] == 'LO') {
                        try {
                            $address = $this->cleanAddress->CleanAddressVerify($address);
                        } catch (\Exception $e) {
                            throw new \Exception('Address verification service is down at this moment. Please try again.');
                        }

                        // Get clean address response and throw error if errorCode is set
                        $cleanAddressResponse = $this->getCleanAddressResponse();

                        if (($cleanAddressResponse['errorCode']) != '') {
                            throw new \MiamiOH\RestngAddressService\Exceptions\CleanAddressFailed('Invalid Address provided.Please look at the Suggestion List');
                        }
                    }
                }

                //Begin transaction to end existing LO record(update) and create new LO record
                $this->dbh->auto_commit(false);

                if ($address['addressType'] == 'LO') {
                    $this->updateAddress->setDBH($this->dbh);
                    $this->updateAddress->endLocalAddress($address['pidm'], $address['addressType']);
                }

                $addressFromDate = (isset($address['fromDate']) && ($address['fromDate']!=='')) ? 'TO_DATE(\''.$this->createFromDateStr($address['fromDate']).'\',\'YYYYMMDD\')' : 'sysdate';
                $addressToDate = (isset($address['toDate']) && ($address['toDate']!=='')) ? 'TO_DATE(\''.$this->createFromDateStr($address['toDate']).'\',\'YYYYMMDD\')' : 'NULL';

                $sequenceNumber = null;
                $userName = $this->getApiUser()->getUsername();
                $dataOrigin = "RESTng Address Service";

                $status = (isset($address['status']) && $address['status'] === 'inactive' && $address['status'] !== '') ? 'I' : null;

                $rowID = '';
                $query = "
                declare

                BEGIN
                GB_ADDRESS.P_CREATE(
                p_pidm  => :P_PIDM,
                p_atyp_code  => :P_ATYP_CODE,
                p_from_date => $addressFromDate,
                p_to_date => $addressToDate,
                p_street_line1 => :P_STREET_LINE1,
                p_street_line2 => :P_STREET_LINE2,
                p_street_line3 => NULL,
                p_city => :P_CITY,
                p_stat_code => :P_STAT_CODE,
                p_zip => :P_ZIP,
                p_cnty_code => NULL,
                p_natn_code => NULL,
                p_status_ind => :P_STATUS_IND,
                p_user => :P_USER,
                p_data_origin => :P_DATA_ORIGIN,
                p_seqno_inout => :P_SEQNO_OUT,
                p_rowid_out => :P_ROWID_OUT,
                p_asrc_code => :P_ASRC_CODE);
                END;";

                $sth = $this->dbh->prepare($query);
                $sth->bind_by_name(':P_PIDM', $address['pidm']);
                $sth->bind_by_name(':P_ATYP_CODE', $address['addressType']);
                $sth->bind_by_name(':P_STREET_LINE1', $address['streetLine1']);
                $sth->bind_by_name(':P_STREET_LINE2', $address['streetLine2']);
                $sth->bind_by_name(':P_CITY', $address['city']);
                $sth->bind_by_name(':P_STAT_CODE', $address['state']);
                $sth->bind_by_name(':P_ZIP', $address['postalCode']);
                $sth->bind_by_name(':P_STATUS_IND', $status);
                $sth->bind_by_name(':P_USER', $userName);
                $sth->bind_by_name(':P_DATA_ORIGIN', $dataOrigin);
                $sth->bind_by_name(':P_SEQNO_OUT', $sequenceNumber, 100);
                $sth->bind_by_name(':P_ROWID_OUT', $rowID, 100);
                $sth->bind_by_name(':P_ASRC_CODE', $address['addressSourceCode']);
                $sth->execute();

                //Commit the changes into database made by transaction
                $this->dbh->commit();
            } catch (\Exception $e) {
                //Rollback the transaction as encountered error when doing either update or create
                $this->dbh->rollback();
                if(strpos($e->getMessage(), 'oci_execute(): ORA-20100: ::Address of this type exists.') !== false){
                    throw new AddressExistsException('Address of this type exists');
                }
                throw $e;
            }

            $address['sequenceNumber'] = $sequenceNumber;
            $address['row_id'] = $rowID;
            $address = $this->makeModelForPostResponse($address);
        } else {
            throw new \Exception("Missing Required Fields : PIDM, AddressType, StreetLine1, City, State, PostalCode");
        }

        return $address;
    }

    /**
     * @param $typeList
     * @return $this
     */
    public function filterType($typeList)
    {
        if (!is_array($typeList)) {
            $typeList = [$typeList];
        }

        $this->filterTypeList = $typeList;

        return $this;
    }

    /**
     * @param $status
     * @return $this
     * @throws \Exception
     */
    public function filterStatus($statusList)
    {
        if (!is_array($statusList)) {
            $statusList = [$statusList];
        }

        $this->filterStatusList = $statusList;

        return $this;
    }

    /**
     * @param $skipValidation
     * @return $this
     * @throws \Exception
     */
    public function filterSkipValidation($skipValidation)
    {
        if (!in_array($skipValidation, ['yes', 'no'])) {
            throw new \Exception('Invalid filter status: ' . $skipValidation);
        }

        $this->filterSkipValidation = $skipValidation;

        return $this;
    }

    /**
     *
     */
    public function clearFilters()
    {
        $this->filterTypeList = [];
        $this->filterStatusList = [];
    }

    /**
     * This function builds the model from record returned by the database query
     *
     * @param $record
     * @return array
     */
    public function makeModelFromRecord($record)
    {
        $model = [];
        $model['pidm'] = $record['spraddr_pidm'];
        $model['addressType'] = $record['spraddr_atyp_code'];
        $model['sequenceNumber'] = $record['spraddr_seqno'];
        $model['fromDate'] = $record['spraddr_from_date'];
        $model['toDate'] = $record['spraddr_to_date'];
        $model['streetLine1'] = $record['spraddr_street_line1'];
        $model['streetLine2'] = $record['spraddr_street_line2'];
        $model['streetLine3'] = $record['spraddr_street_line3'];
        $model['city'] = $record['spraddr_city'];
        $model['state'] = $record['spraddr_stat_code'];
        $model['postalCode'] = $record['spraddr_zip'];
        $model['nationBannerCode'] = $record['spraddr_natn_code'];
        $model['nationDescription'] = $record['stvnatn_nation'];
        $model['status'] = isset($record['spraddr_status_ind']) && $record['spraddr_status_ind'] === 'I' ? 'inactive' : 'active';
        $model['id'] = $record['row_id'];
        $model['addressSourceCode'] = $record['spraddr_asrc_code'];

        return $model;
    }

    /**
     * @param $address
     */
    public function makeModelForPostResponse($address)
    {
        $values = [];
        $queryString = "SELECT spraddr_pidm,
                               spraddr_atyp_code,
                               spraddr_status_ind,
                               to_char(spraddr_from_date,'YYYY-MM-DD') as spraddr_from_date,
                               to_char(spraddr_to_date, 'YYYY-MM-DD') as spraddr_to_date,
                               spraddr_street_line1,
                               spraddr_street_line2,
                               spraddr_street_line3,
                               spraddr_city,
                               spraddr_stat_code,
                               spraddr_zip,
                               spraddr_natn_code,
                               stvnatn_nation,
                               spraddr_seqno,
                               spraddr_asrc_code,
                               ROWIDTONCHAR(spraddr.ROWID) as row_id
                               FROM SPRADDR left join stvnatn on stvnatn_code = spraddr_natn_code 
                               WHERE spraddr_pidm = ? AND spraddr_atyp_code = ? AND spraddr_seqno = ?";
        $values[] = $address['pidm'];
        $values[] = $address['addressType'];
        $values[] = $address['sequenceNumber'];

        $records = $this->dbh->queryall_array($queryString, $values);

        // queryall_array returns only one record which is just now created in create() method
        $address = $this->makeModelFromRecord($records[0]);

        return $address;
    }

    public function validateDate($date, $format = self::DATE_FORMAT)
    {
        $dateObject = Carbon::createFromFormat($format, $date);
        return $dateObject && $dateObject->format($format) === $date;
    }

    public function createFromDateStr($dateString)
    {
        if ($this->validateDate($dateString)) {
            return Carbon::createFromFormat(self::DATE_FORMAT, $dateString)->format('Ymd');
        } else {
            throw new \Exception("Invalid Date format, It should be in the format " . self::DATE_FORMAT);
        }
    }

    public function deleteAddressData($id): void
    {
        $query = '
                    declare
                    BEGIN
                    GB_ADDRESS.P_DELETE(
                        p_pidm => NULL,
                        p_atyp_code => NULL,
                        p_seqno => NULL,
                        p_rowid => :P_ROW_ID
                    );
                    END;';
        $sth = $this->dbh->prepare($query);
        $sth->bind_by_name(':P_ROW_ID', $id);
        $sth->execute();
    }
}
