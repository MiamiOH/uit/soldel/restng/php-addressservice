<?php

namespace MiamiOH\RestngAddressService\Services;

use MiamiOH\RESTng\App;
use MiamiOH\RestngAddressService\Exceptions\AddressExistsException;
use MiamiOH\RestngAddressService\Exceptions\PidmNotFoundException;

class Addressv2REST extends \MiamiOH\RESTng\Service
{

    /** @var \MiamiOH\RestngAddressService\Services\Addressv2 $address */
    private $address;

    private $dbh;
    private $dataSourceName = 'MUWS_GEN_PROD';

    /**
     * @param $database
     */
    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->dataSourceName);
    }

    /**
     * @param $address
     */
    public function setAddressv2($address)
    {
        $this->address = $address;
    }

    /**
     * @param $bannerUtil
     */
    public function setBannerUtil($bannerUtil)
    {
        /** @var \MiamiOH\RESTng\Service\Extension\BannerUtil $bannerUtil */
        $this->bannerUtil = $bannerUtil;
    }

    /**
     * This functions returns the collection of address records based
     * on address type and status indicator filter.
     *
     * @return mixed
     */
    public function getAddressv2()
    {
        //log
        $this->log->debug('Start the addressv2REST service.');
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();

        try {
            $pidm = $this->getValidPidm($request);

            if (isset($options['status']) && $options['status']) {
                $this->address->filterStatus($options['status']);
            }

            if (isset($options['addressType']) && $options['addressType']) {
                $this->address->filterType($options['addressType']);
            }

            $payload = $this->address->read($pidm);

            $response->setStatus(\MiamiOH\RESTng\App::API_OK);

        } catch (PidmNotFoundException $e) {
            $this->log->info($e->getMessage());
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            $createAddressResult = [
                'status' => \MiamiOH\RESTng\App::API_NOTFOUND,
                'message' => 'Pidm not found',
                'address' => [],
            ];
            $payload[] = $createAddressResult;
        } catch (\MiamiOH\RESTng\Service\Extension\BannerIdNotFound $e) {
            $this->log->info($e->getMessage());
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
        } catch (\Exception $e) {
            $this->log->error($e->getMessage());
            $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
        }
        $response->setPayload($payload);

        return $response;
    }

    public function getAddressv2Collection()
    {
        //log
        $this->log->debug('Start the addressv2REST service.');
        $request = $this->getRequest();
        $response = $this->getResponse();

        $options = $request->getOptions();

        // Refuse to return full collection; pidm filter must be set
        if (!isset($options['pidm'])) {
            $response->setStatus(\MiamiOH\RESTng\App::API_BADREQUEST);
            return $response;
        }

        if (isset($options['status']) && $options['status']) {
            $this->address->filterStatus($options['status']);
        }

        if (isset($options['addressType']) && $options['addressType']) {
            $this->address->filterType($options['addressType']);
        }


        $payload = $this->address->read($options['pidm']);

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);

        return $response;
    }

    public function postAddressv2Collection()
    {
        //log
        $this->log->debug('Start the addressv2REST service.');
        $request = $this->getRequest();
        $response = $this->getResponse();
        $addressList = $request->getData();
        $options = $request->getOptions();

        try {
            $pidm = $this->getValidPidm($request);
        } catch (\MiamiOH\RESTng\Service\Extension\BannerIdNotFound $e) {
            $this->log->info($e->getMessage());
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            $createAddressResult = [
                'status' => \MiamiOH\RESTng\App::API_NOTFOUND,
                'message' => 'No matching PIDM for the muid provided',
                'address' => [],
            ];
            $payload[] = $createAddressResult;
        } catch (PidmNotFoundException $e) {
            $this->log->info($e->getMessage());
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            $createAddressResult = [
                'status' => \MiamiOH\RESTng\App::API_NOTFOUND,
                'message' => 'Pidm not found',
                'address' => [],
            ];
            $payload[] = $createAddressResult;
        }

        if (isset($pidm)) {
            for ($i = 0; $i < count($addressList); $i++) {
                $createAddressResult = [
                    'status' => \MiamiOH\RESTng\App::API_CREATED,
                    'message' => 'POST Success',
                    'address' => [],
                    'errorCode' => '',
                    'errorString' => '',
                    'suggestionList' => '',
                ];

                if (!isset($addressList[$i]['pidm'])) {
                    $addressList[$i]['pidm'] = $pidm;
                }

                if ($addressList[$i]['pidm'] !== $pidm) {
                    $this->log->info('Resource ID does not match PIDM in address model');
                    $createAddressResult['status'] = \MiamiOH\RESTng\App::API_BADREQUEST;
                    $createAddressResult['message'] = 'Resource ID does not match PIDM in address model';
                } else {
                    try {
                        if (isset($options['skipValidation']) && $options['skipValidation']) {
                            $this->address->filterSkipValidation($options['skipValidation']);
                        }
                        $createAddressResult['address'] = $this->address->create($addressList[$i]);
                    } catch (AddressExistsException $e) {
                        $this->log->info($e->getMessage());
                        $response->setStatus(\MiamiOH\RESTng\App::API_BADREQUEST);
                        $createAddressResult = [
                            'status' => \MiamiOH\RESTng\App::API_BADREQUEST,
                            'message' => 'Address of this type exists',
                            'address' => [],
                        ];
                    } catch (\MiamiOH\RestngAddressService\Exceptions\CleanAddressFailed $e) {
                        $this->log->info($e->getMessage());
                        $createAddressResult['status'] = \MiamiOH\RESTng\App::API_BADREQUEST;
                        $createAddressResult['message'] = $e->getMessage();

                        // Init clean address response message
                        $clnAddressResponse = [
                            'errorCode' => '',
                            'errorString' => '',
                            'suggestionList' => '',
                        ];

                        $clnAddressResponse = $this->address->getCleanAddressResponse();

                        /* This exception is raised when clean address fails.
                           So copy the clean address response message into REST response. */
                        $createAddressResult['errorCode'] = $clnAddressResponse['errorCode'];
                        $createAddressResult['errorString'] = $clnAddressResponse['errorString'];
                        $createAddressResult['suggestionList'] = $clnAddressResponse['suggestionList'];
                    } catch (\MiamiOH\RESTng\Exception\BadRequest $e) {
                        $this->log->info($e->getMessage());
                        $createAddressResult['status'] = \MiamiOH\RESTng\App::API_BADREQUEST;
                        $createAddressResult['message'] = $e->getMessage();
                    } catch (\Exception $e) {
                        $this->log->error($e);
                        $createAddressResult['status'] = \MiamiOH\RESTng\App::API_FAILED;
                        $createAddressResult['message'] = $e->getMessage();
                    }
                }
                $payload[] = $createAddressResult;
            }
            $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        }
        $response->setPayload($payload);

        return $response;
    }

    private function getValidPidm($request)
    {
        if ($request->getResourceParamKey('muid') === 'pidm') {
            $pidm = $this->dbh->queryfirstrow_assoc("select SPRIDEN_PIDM from spriden where SPRIDEN_PIDM = ?",
                $request->getResourceParam('muid'));
            if (isset($pidm['spriden_pidm'])) {
                return $pidm['spriden_pidm'];
            } else {
                throw new PidmNotFoundException('Pidm not found');
            }
        } else {
            $bannerId = $this->bannerUtil->getId(
                $request->getResourceParamKey('muid'),
                $request->getResourceParam('muid')
            );
            return $bannerId->getPidm();
        }
    }

    public function deleteAddressv2()
    {
        //log
        $this->log->debug('Start the addressv2REST service.');
        $request = $this->getRequest();
        $response = $this->getResponse();
        $status = App::API_OK;
        $data = [];
        try {
            $id = $request->getResourceParam('id');

            $this->address->deleteAddressData($id);
        } catch (\MiamiOH\RESTng\Exception\BadRequest $e) {
            $status = \MiamiOH\RESTng\App::API_BADREQUEST;
            $data['message'] = $e->getMessage();
        } catch (\Exception $e) {
            $status = \MiamiOH\RESTng\App::API_FAILED;
            $data['message'] = $e->getMessage();
        }
        $response->setPayload($data);
        $response->setStatus($status);
        return $response;
    }


    public function updateAddressv2()
    {
        $this->log->debug('Start the addressv2REST service.');
        $request = $this->getRequest();
        $response = $this->getResponse();
        /*
        $options = $request->getOptions();
        $payload = $request->getData();

        try {
            $bannerId = $this->bannerUtil->getId($request->getResourceParamKey('muid'),
                $request->getResourceParam('muid'));
            $pidm = $bannerId->getPidm();
            $addressCode = '';
            if (isset($options['addressCode']) && $options['addressCode']){
                $addressCode = strtoupper($options['addressCode']);
            }

            $status = $this->address->updateAddressData($pidm,$addressCode,$payload);

            $response->setStatus(\MiamiOH\RESTng\App::API_OK);

        } catch (\MiamiOH\RESTng\Service\Extension\BannerIdNotFound $e) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
        }
        */
        $response->setStatus(\MiamiOH\RESTng\App::API_NOTIMPLEMENTED);
        return $response;
    }
}
