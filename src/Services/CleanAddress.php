<?php

namespace MiamiOH\RestngAddressService\Services;

class CleanAddress extends \MiamiOH\RESTng\Service
{
    private $dbh;

    private $dataSourceName = 'MUWS_GEN_PROD';

    private $cleanAddressResponse = [];

    /**
     * @param $database
     */
    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->dataSourceName);
    }

    public function initCleanAddressResponse()
    {
        $this->cleanAddressResponse = [
            'errorCode' => '',
            'errorString' => '',
            'suggestionList' => '',
        ];
    }


    /**
     * @param $clnAddressRes
     */
    public function setCleanAddressResponse($clnAddressRes)
    {
        $this->cleanAddressResponse['errorCode'] = $clnAddressRes['errorCode'];
        $this->cleanAddressResponse['errorString'] = $clnAddressRes['errorString'];
        $this->cleanAddressResponse['suggestionList'] = $clnAddressRes['suggestionList'];
    }

    /**
     * This method returns the clean address response
     */
    public function getCleanAddressResponse()
    {
        return $this->cleanAddressResponse;
    }

    /*
     * This method verifies the address and returns the standardized
     *  and verified address components
     *
     */
    public function cleanAddressVerify($address)
    {
        $clnAddress = [];
        $streetLine1 = '';
        $streetLine2 = '';
        $city = '';
        $state = '';
        $postalCode = '';
        $postalCodeExt = '';
        $errorCode = '';
        $errorString = '';
        $suggestionList = '';

        // Init the clean address response
        $this->initCleanAddressResponse();

        // fn_error_suggest_count is set 100(suggestion list will not exceed this limit) to retrieve all suggestions
        $query = "
            declare
            
            AddressRec CLEAN_ADDRESS.ADDRESS_REC;
            BEGIN
            AddressRec.Address_Line_1 := :ADDRESSLINE1;
            AddressRec.Address_Line_2 := :ADDRESSLINE2;
            AddressRec.City := :CITY;
            AddressRec.State := :STATE;
            AddressRec.Postal_Code := :POSTALCODE;

            CLEAN_ADDRESS.Verify(
            f_address_rec  => AddressRec,
            fn_error_suggest_count => 100);
            :StreetLine1Out := AddressRec.Address_Line_1;
            :StreetLine2Out := AddressRec.Address_Line_2;
            :CityOut := AddressRec.City;
            :StateOut := AddressRec.State;
            :PostalCodeOut := AddressRec.Postal_Code;
            :PostalCodeExtOut := AddressRec.Postal_Code_Extended;
            :ErrorCodeOut := AddressRec.Error_Code;
            :ErrorStringOut := AddressRec.Error_String;
            :SuggestionListOut := AddressRec.Suggestion_List;
            END;";

        $sth = $this->dbh->prepare($query);

        // Input parameter binding
        $sth->bind_by_name(':ADDRESSLINE1', $address['streetLine1']);
        $sth->bind_by_name(':ADDRESSLINE2', $address['streetLine2']);
        $sth->bind_by_name(':CITY', $address['city']);
        $sth->bind_by_name(':STATE', $address['state']);
        $sth->bind_by_name(':POSTALCODE', $address['postalCode']);

        // Output parameter binding
        $sth->bind_by_name(':StreetLine1Out', $streetLine1, 256);
        $sth->bind_by_name(':StreetLine2Out', $streetLine2, 256);
        $sth->bind_by_name(':CityOut', $city, 100);
        $sth->bind_by_name(':StateOut', $state, 100);
        $sth->bind_by_name(':PostalCodeOut', $postalCode, 20);
        $sth->bind_by_name(':PostalCodeExtOut', $postalCodeExt, 10);
        $sth->bind_by_name(':ErrorCodeOut', $errorCode, 20);
        $sth->bind_by_name(':ErrorStringOut', $errorString, 4000);
        $sth->bind_by_name(':SuggestionListOut', $suggestionList, 4000);

        $sth->execute();

        /* Build the address record if $errorCode is null.
           It indicates that there are only minor corrections in the address provided
           and need to update the input address record. */
        if ($errorCode == '') {
            $address['streetLine1'] = $streetLine1;
            $address['streetLine2'] = $streetLine2;
            $address['city'] = $city;
            $address['state'] = $state;
            $address['postalCode'] = $postalCode;
            // Append the postalCodeExt only if it is set
            if (isset($postalCodeExt)) {
                $address['postalCode'] = $postalCode . "-" . $postalCodeExt;
            }
            $clnAddress['errorCode'] = $errorCode;
            $clnAddress['errorString'] = $errorString;
            $clnAddress['suggestionList'] = $suggestionList;
        } else {
            // Build the create address result with list of address suggested by clean address
            // and error code
            $clnAddress['errorCode'] = $errorCode;
            $clnAddress['errorString'] = $errorString;
            $clnAddress['suggestionList'] = $suggestionList;
        }
        $this->setCleanAddressResponse($clnAddress);

        return $address;
    }
}
