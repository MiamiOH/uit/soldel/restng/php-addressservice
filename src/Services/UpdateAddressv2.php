<?php

namespace MiamiOH\RestngAddressService\Services;

class UpdateAddressv2 extends \MiamiOH\RESTng\Service
{
    private $dbh;

    /*
     * This is method is called to set the database handle
     * from Addressv2.class.php createAddressData() method.
     * Same database handle is used between both the classes
     * due to transaction.
     *
     * Input:
     * Database handle
     */
    public function setDBH($dbh)
    {
        $this->dbh = $dbh;
    }

    /*
     * This method is called to end the previous active LO records
     * before creating new LO address record.
     *
     */
    public function endLocalAddress($pidm, $addressType)
    {
        $values[] = $pidm;
        $values[] = $addressType;

        // End all active previous records including future address by setting status-ind as I
        $queryString = "UPDATE spraddr SET spraddr_status_ind='I' 
                        WHERE spraddr_pidm = ? AND spraddr_atyp_code = ? AND 
                        (spraddr_status_ind is null OR spraddr_status_ind = 'A')";

        $sth = $this->dbh->perform($queryString, $values);
    }
}
