<?php
/*
-----------------------------------------------------------
FILE NAME: addressGetTest.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Emily Schmidt

DESCRIPTION:  Unit Tests for Testing the GET Functionality of the Address Web Service

ENVIRONMENT DEPENDENCIES: PHP Unit

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

10/12/2015       SCHMIDEE
Description:  Initial Program

11/19/2015      AXHAY
Description:  Added comments whereever required.

11/23/2015      AXHAY
Description:  Added the code for check payload, test for multipidms and modified the function name wherever required to maintain consistency

11/24/2015      AXHAY
Description:  Code clean up

12/9/2015       AXHAY
Description:  Changed the snake case response to the camel case to maintain consistency.

 */

namespace MiamiOH\RestngAddressService\Tests\Unit;

use MiamiOH\RESTng\App;

class addressGetTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $dbh, $address, $queryallRecords, $user, $request;

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {

        //set up the mock api:
        $api = $this->createMock(App::class);


        $api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        //set up the mock database:
        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        //set up the mock user:
        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        //set up the mock datasource:
        $ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->disableOriginalConstructor()
            ->setMethods(array('getDataSource'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->address = new \MiamiOH\RestngAddressService\Services\Address();
        $this->address->setApp($api);                  //where is the method defined? /opt/webapps/wc/restng/src/RESTng/Service/Person/Address
        $this->address->setDatabase($db);
        $this->address->setApiUser($this->user);      //where is the method defined? /opt/webapps/wc/restng/src/RESTng/Service/Person/Address

        $this->address->setDatasource($ds);
        $this->address->setRequest($this->request);  //where is the method defined? /opt/webapps/wc/restng/src/RESTng/Service/Person/Address

    }


    /*************************/
    /**********Tests**********/
    /*************************/

    /*
     *	Retrive a Single PIDM Test
     *	Tests the Retrival of a Single PIDM
     *	Expected Return: 200 OK Response with a payload consisting of 2 elements that
                         matches the array in the singlePidmReturn method.
     */
    public function testGetAddressSinglePidm()
    {     //renamed the method

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();
        $request->expects($this->once())->method('getOptions')->willReturn(array('pidm' => array($this->queryall_arrayAddressInfoSinglePidm()[0]['spraddr_pidm']), 'token' => 'blahblahblah'));
        $this->address->setRequest($request);

        //tell the user what to do when the isAuthorized method is called.
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'mockAuthorizedUser')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'queryall_arrayAddressInfoSinglePidm')));

        //get the response and payload from the getSchedule() method.
        $response = $this->address->getAddress();
        $payload = $response->getPayload();

        /***Make assertions***/
        // Check the status and general response
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $response->getStatus());
        $this->assertTrue(is_array($payload));

        //$this->assertEquals(count($payload), 2);
        //print_r($payload);
        //	$this->assertEquals($payload, $this->singlePidmReturn());

        // Check payload
        $this->assertEquals($this->queryall_arrayAddressInfoSinglePidm()[0]['spraddr_pidm'], $payload['address0']['pidm']);
        $this->assertEquals($this->queryall_arrayAddressInfoSinglePidm()[0]['spraddr_atyp_code'], $payload['address0']['addressCode']);
        $this->assertEquals($this->queryall_arrayAddressInfoSinglePidm()[0]['spraddr_street_line1'], $payload['address0']['addressLine1']);
        $this->assertEquals($this->queryall_arrayAddressInfoSinglePidm()[0]['spraddr_street_line2'], $payload['address0']['addressLine2']);
        $this->assertEquals($this->queryall_arrayAddressInfoSinglePidm()[0]['spraddr_city'], $payload['address0']['city']);
        $this->assertEquals($this->queryall_arrayAddressInfoSinglePidm()[0]['spraddr_stat_code'], $payload['address0']['state']);
        $this->assertEquals($this->queryall_arrayAddressInfoSinglePidm()[0]['spraddr_zip'], $payload['address0']['zip']);
        $this->assertEquals($this->queryall_arrayAddressInfoSinglePidm()[0]['spraddr_seqno'], $payload['address0']['sequenceNumber']);

        $this->assertEquals($this->queryall_arrayAddressInfoSinglePidm()[1]['spraddr_pidm'], $payload['address1']['pidm']);
        $this->assertEquals($this->queryall_arrayAddressInfoSinglePidm()[1]['spraddr_atyp_code'], $payload['address1']['addressCode']);
        $this->assertEquals($this->queryall_arrayAddressInfoSinglePidm()[1]['spraddr_street_line1'], $payload['address1']['addressLine1']);
        $this->assertEquals($this->queryall_arrayAddressInfoSinglePidm()[1]['spraddr_street_line2'], $payload['address1']['addressLine2']);
        $this->assertEquals($this->queryall_arrayAddressInfoSinglePidm()[1]['spraddr_city'], $payload['address1']['city']);
        $this->assertEquals($this->queryall_arrayAddressInfoSinglePidm()[1]['spraddr_stat_code'], $payload['address1']['state']);
        $this->assertEquals($this->queryall_arrayAddressInfoSinglePidm()[1]['spraddr_zip'], $payload['address1']['zip']);
        $this->assertEquals($this->queryall_arrayAddressInfoSinglePidm()[1]['spraddr_seqno'], $payload['address1']['sequenceNumber']);

    }

    public function testGetAddressMultiPidm()
    {

        /*** set up to call the getEmail method ***/
        //Create the mock request object, define it's getOptions method and pass it into the email object.
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();
        $request->expects($this->once())->method('getOptions')->willReturn(array('pidm' => array($this->mockDbRecordsMultiPidm[0]['spraddr_pidm'], $this->mockDbRecordsMultiPidm[3]['spraddr_pidm']), 'token' => 'blahblah'));
        $this->address->setRequest($request);

        //define what we want the isAuthorized method to return for this method
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'authorizedUser')));
        //->will($this->returnCallback(array($this, 'notAuthorizedUser'))); //for unauthorized user testing

        //define the querry_all method for this test
        $this->dbh->method('queryall_array')
            ->willReturn($this->mockDbRecordsMultiPidm);

        /*** Call the address and get the payload ***/
        $resp = $this->address->getAddress();
        $payload = $resp->getPayload();

        /*** Make assertions ***/
        //check status and general response
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));

        //check payload
        $this->assertEquals($this->mockDbRecordsMultiPidm[0]['spraddr_pidm'], $payload['address0']['pidm']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[0]['spraddr_atyp_code'], $payload['address0']['addressCode']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[0]['spraddr_street_line1'], $payload['address0']['addressLine1']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[0]['spraddr_street_line2'], $payload['address0']['addressLine2']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[0]['spraddr_city'], $payload['address0']['city']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[0]['spraddr_stat_code'], $payload['address0']['state']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[0]['spraddr_zip'], $payload['address0']['zip']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[0]['spraddr_seqno'], $payload['address0']['sequenceNumber']);

        $this->assertEquals($this->mockDbRecordsMultiPidm[1]['spraddr_pidm'], $payload['address1']['pidm']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[1]['spraddr_atyp_code'], $payload['address1']['addressCode']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[1]['spraddr_street_line1'], $payload['address1']['addressLine1']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[1]['spraddr_street_line2'], $payload['address1']['addressLine2']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[1]['spraddr_city'], $payload['address1']['city']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[1]['spraddr_stat_code'], $payload['address1']['state']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[1]['spraddr_zip'], $payload['address1']['zip']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[1]['spraddr_seqno'], $payload['address1']['sequenceNumber']);

        $this->assertEquals($this->mockDbRecordsMultiPidm[2]['spraddr_pidm'], $payload['address2']['pidm']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[2]['spraddr_atyp_code'], $payload['address2']['addressCode']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[2]['spraddr_street_line1'], $payload['address2']['addressLine1']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[2]['spraddr_street_line2'], $payload['address2']['addressLine2']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[2]['spraddr_city'], $payload['address2']['city']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[2]['spraddr_stat_code'], $payload['address2']['state']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[2]['spraddr_zip'], $payload['address2']['zip']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[2]['spraddr_seqno'], $payload['address2']['sequenceNumber']);

        $this->assertEquals($this->mockDbRecordsMultiPidm[3]['spraddr_pidm'], $payload['address3']['pidm']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[3]['spraddr_atyp_code'], $payload['address3']['addressCode']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[3]['spraddr_street_line1'], $payload['address3']['addressLine1']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[3]['spraddr_street_line2'], $payload['address3']['addressLine2']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[3]['spraddr_city'], $payload['address3']['city']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[3]['spraddr_stat_code'], $payload['address3']['state']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[3]['spraddr_zip'], $payload['address3']['zip']);
        $this->assertEquals($this->mockDbRecordsMultiPidm[3]['spraddr_seqno'], $payload['address3']['sequenceNumber']);

    }

    /*
     *	Retrive a Single PIDM and no Results are Found TEST
     *	Tests the Retrival of a Single PIDM where no results are found.
     *	Expected Return: 200 OK with a Empty Array Results.
     */

    public function testGetAddressWithNoPidm()
    {    // renamed the method

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'queryall_arrayEmpty')));

        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'mockAuthorizedUser')));

        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSinglePidm')));

        //get the response and payload from the getSchedule() method.
        $response = $this->address->getAddress();
        $payload = $response->getPayload();
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $response->getStatus());
        //	$this->assertEquals(count($payload), 0);
        $this->assertEquals($payload, array());
    }

    /*
       *	Invalid User Test
       * 	Tests Case in which a user is not Authorized to use this service.
       *	Expected Return: 401 Forbidden Error
       */
    public function testInvalidAddressAuthorization()
    {

        //tell the dbh what to do when isAuthorized method is called.
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'mockNotAuthorizedUser')));
        $response = $this->address->getAddress();

        //get the response and payload from the getStatus() method.
        $this->assertEquals(\MiamiOH\RESTng\App::API_UNAUTHORIZED, $response->getStatus());
    }

    /** Helper methods **/

    public function authorizedUser()
    {
        return true;
    }

    /*************************/
    /**Start of Mock Methods**/
    /*************************/
    public function mockAuthorizedUser()
    {
        return true;
    }

    public function mockNotAuthorizedUser()
    {
        return false;
    }

    public function mockOptionsSinglePidm()
    {
        $optionsArray = array('pidm' => array('123456789'));
        return $optionsArray;
    }

    public function mockOptionsSingleUniqueID()
    {
        $optionsArray = array('uniqueid' => array('testUser'));
        return $optionsArray;
    }

    public function mockOptionsNoOptions()
    {
        $optionsArray = array();
        return $optionsArray;
    }

    public function mockOptionsNoPIDMS()
    {
        $optionsArray = array('pidm' => array());
        return $optionsArray;
    }

    /* Unique ID was removed
    public function mockOptionsNoUniqueID() {
        $optionsArray = array('uniqueid'=> array());
        return $optionsArray;
    }
    */
    public function mockOptionJunkPIDMS()
    {
        $optionsArray = array('pidm' => array('000000'));
        return $optionsArray;
    }

    /* Unique ID Was removed
    public function mockOptionsJunkUniqueID() {
        $optionsArray = array('uniqueid'=> array('JunkID'));
        return $optionsArray;
    }
    */
    public function singlePidmReturn()
    {
        $expectedReturn = array(
            'address0' =>
                array(
                    'pidm' => '123456789',
                    'addressCode' => 'MA',
                    'addressLine1' => '123 nowhere dr.',
                    'addressLine2' => '',
                    'city' => 'nowhere',
                    'state' => 'oh',
                    'zip' => '12345',
                    'spraddr_natn_code' => '',
                    'stvnatn_nation' => '',
                    'sequenceNumber' => '1'
                ),
            'address1' =>
                array(
                    'pidm' => '123456789',
                    'addressCode' => 'BU',
                    'addressLine1' => '234 nowhere dr.',
                    'addressLine2' => 'apt 1',
                    'city' => 'nowhere',
                    'state' => 'oh',
                    'zip' => '67890',
                    'spraddr_natn_code' => '',
                    'stvnatn_nation' => '',
                    'sequenceNumber' => '2'
                ),

        );
        return $expectedReturn;
    }

    public function queryall_arrayAddressInfoSinglePidm()
    {
        //$this->queryallRecords = array(
        $mockDbRecordsSinglePidm = array( //array of 3 database records
            array( //db record 1
                'spraddr_pidm' => '104266',
                'spraddr_atyp_code' => 'MA',
                'spraddr_street_line1' => '14607 dawn vale dr',
                'spraddr_street_line2' => '',
                'spraddr_city' => 'houston',
                'spraddr_stat_code' => 'tx',
                'spraddr_zip' => '77062-2128',
                'spraddr_natn_code' => '',
                'stvnatn_nation' => '',
                'spraddr_seqno' => '1'
            ),

            array( //db record 2
                'spraddr_pidm' => '104267',
                'spraddr_atyp_code' => 'MA',
                'spraddr_street_line1' => '1102 fourteenth st',
                'spraddr_street_line2' => '',
                'spraddr_city' => 'middletown',
                'spraddr_stat_code' => 'oh',
                'spraddr_zip' => '45044',
                'spraddr_natn_code' => '',
                'stvnatn_nation' => '',
                'spraddr_seqno' => '1'
            ));

        return $mockDbRecordsSinglePidm;

    }

    private $mockDbRecordsMultiPidm = array( //array of 4 database records
        array( //db record 1
            'spraddr_pidm' => '104268',
            'spraddr_atyp_code' => 'MA',
            'spraddr_street_line1' => 'washington park',
            'spraddr_street_line2' => '',
            'spraddr_city' => 'oxford',
            'spraddr_stat_code' => 'ny',
            'spraddr_zip' => '13830',
            'spraddr_natn_code' => '',
            'stvnatn_nation' => '',
            'spraddr_seqno' => '1'
        ),
        array( //db record 2
            'spraddr_pidm' => '123456789',
            'spraddr_atyp_code' => 'MA',
            'spraddr_street_line1' => '123 nowhere dr.',
            'spraddr_street_line2' => '',
            'spraddr_city' => 'nowhere',
            'spraddr_stat_code' => 'oh',
            'spraddr_zip' => '12345',
            'spraddr_natn_code' => '',
            'stvnatn_nation' => '',
            'spraddr_seqno' => '1'
        ),
        array( //db record 3
            'spraddr_pidm' => '123456789',
            'spraddr_atyp_code' => 'MA',
            'spraddr_street_line1' => '123 nowhere dr.',
            'spraddr_street_line2' => '',
            'spraddr_city' => 'nowhere',
            'spraddr_stat_code' => 'oh',
            'spraddr_zip' => '12345',
            'spraddr_natn_code' => '',
            'stvnatn_nation' => '',
            'spraddr_seqno' => '1'
        ),
        array( //db record 4
            'spraddr_pidm' => '123456789',
            'spraddr_atyp_code' => 'MA',
            'spraddr_street_line1' => '123 nowhere dr.',
            'spraddr_street_line2' => '',
            'spraddr_city' => 'nowhere',
            'spraddr_stat_code' => 'oh',
            'spraddr_zip' => '12345',
            'spraddr_natn_code' => '',
            'stvnatn_nation' => '',
            'spraddr_seqno' => '1'
        ));

    public function queryall_arrayEmpty()
    {
        return array();
    }

}
