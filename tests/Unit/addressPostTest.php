<?php
/*
-----------------------------------------------------------
FILE NAME: addressPostTest.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Emily Schmidt

DESCRIPTION:  Unit Tests for Testing the POST Functionality of the Address Web Service

ENVIRONMENT DEPENDENCIES: PHP Unit

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

10/12/2015       SCHMIDEE
Description:  Initial Program

11/25/2015       AXHAY
Description:  Code review, code modification to reflect message instead of count of success and failure.

12/9/2015        AXHAY
Description:  Changed the snake case response to camel case.

 */

namespace MiamiOH\RestngAddressService\Tests\Unit;

use MiamiOH\RESTng\App;

class addressPostTest extends \MiamiOH\RESTng\Testing\TestCase
{

    //////////
    //Set Up//
    //////////
    private $dbh, $address, $queryallRecords, $user;

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {

        //set up the mock api:
        $api = $this->createMock(App::class);
        $api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array', 'perform'))
            ->getMock();

        //set up the mock database:
        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        //set up the mock user:
        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized', 'getUsername'))
            ->getMock();

        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'returnResourceParam')));

        $request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptions')));


        //set up the mock data source
        $ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->disableOriginalConstructor()
            ->setMethods(array('getDataSource'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->address = new \MiamiOH\RestngAddressService\Services\Address();
        $this->address->setApp($api);
        $this->address->setDatabase($db);
        $this->address->setApiUser($this->user);
        $this->address->setDatasource($ds);
        $this->address->setRequest($request);

    }


    /*************************/
    /**********Tests**********/
    /*************************/

    /*
     *	Invalid User Test
     * 	Tests Case in which a user is not Authorized to use this service.
     *	Expected Return: 401 Forbidden Error
     */
    public function testInvalidAuthorization()
    {
        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();
        $request->method('getData')
            ->will($this->returnCallback(array($this, 'mockPostBody')));

        $this->user->method('isAuthorized')
            ->willReturn(false);

        $this->user->method('getUsername')
            ->will($this->returnCallback(array($this, 'mockAuthorizedUsername')));

        $this->address->setRequest($request);

        $response = $this->address->postaddress();

        //get the response and payload from the getSchedule() method.
        $this->assertEquals(\MiamiOH\RESTng\App::API_UNAUTHORIZED, $response->getStatus());
    }

    /*
     *	Post a Single PIDM Test
     *	Tests the Post of a Single PIDM
     *	Expected Return: 200 OK Response with a payload consisting of 4 elements that
                         matches the array in the mockSuccessfulSinglePostReturn method.
     */
    public function testPostAddressSinglePidm()
    {

        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();
        $request->method('getData')
            ->will($this->returnCallback(array($this, 'mockPostBody')));


        //define what we want isAuthorized method to return
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'mockAuthorizedUser')));
        $this->user->method('getUsername')
            ->will($this->returnCallback(array($this, 'mockAuthorizedUsername')));
        $this->dbh->method('perform')
            ->will($this->returnCallback(array($this, 'mockPerform')));
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockErrorCheckOkSinglePost')));

        /*** Call the postAddress and get the payload ***/

        $this->address->setRequest($request);
        $response = $this->address->postAddress();
        $payload = $response->getPayload();

        /*** Make assertions about the response from the postAddress method ***/
        // $this->assertEquals($payload[0]['pidm'], '123456789');
        //$this->assertEquals($payload[0]['code'], '201');
        //	$this->assertEquals($payload[0]['message'], 'Address successfully inserted.');

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $response->getStatus());
        $this->assertEquals(count($payload[0]), 3);
        $this->assertEquals($payload[0], $this->mockSuccessfulSinglePostReturn());

    }

    /*
     *	Confidentialty Flag Test
     *	Tests the Post of a Single PIDM and the error check returns a confidentiality flag
     *	of Y
     *	Expected Return: 200 OK Response with a payload consisting of 4 elements that
                         matches the array in the mockFailedConfidentialityCheckReturn method.
     */
    public function testPostAddressSinglePidmConfidentiality()
    {

        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();

        $request->method('getData')
            ->will($this->returnCallback(array($this, 'mockPostBody')));
        $this->address->setRequest($request);

        //define what we want isAuthorized method to return
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'mockAuthorizedUser')));
        $this->user->method('getUsername')
            ->will($this->returnCallback(array($this, 'mockAuthorizedUsername')));
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockErrorCheckConfidentialSinglePost')));
        $this->dbh->method('perform')
            ->will($this->returnCallback(array($this, 'mockPerform')));

        /*** Call the postAddress and get the payload ***/
        $response = $this->address->postAddress();
        $payload = $response->getPayload();

        /*** Make assertions about the response from the postAddress method ***/
        //$this->assertEquals($payload[0]['pidm'], '123456789');
        //$this->assertEquals($payload[0]['code'], '500');
        //$this->assertEquals($payload[0]['message'], 'There was a database error when attempting to insert the record.');

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $response->getStatus());
        $this->assertEquals(count($payload[0]), 3);
        $this->assertEquals($payload[0], $this->mockFailedConfidentialityCheckReturn());

    }

    /*
     *	Duplicate Number Flag Test
     *	Tests the Post of a Single PIDM where the number trying to insert is a duplicate.
     *	Expected Return: 200 OK Response with a payload consisting of 4 elements that
                         matches the array in the mockFailedDuplicateCheckReturn method.
     */
    public function testErrorCheckDuplicateSinglePost()
    {

        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();
        $request->method('getData')
            ->will($this->returnCallback(array($this, 'mockPostBody')));
        $this->address->setRequest($request);

        //define what we want isAuthorized method to return
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'mockAuthorizedUser')));
        $this->user->method('getUsername')
            ->will($this->returnCallback(array($this, 'mockAuthorizedUsername')));
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockErrorCheckDuplicateSinglePost')));
        $this->dbh->method('perform')
            ->will($this->returnCallback(array($this, 'mockPerform')));

        /*** Call the postAddress and get the payload ***/
        $response = $this->address->postAddress();
        $payload = $response->getPayload();

        /*** Make assertions about the response from the postAddress method ***/
        //$this->assertEquals($payload[0]['pidm'], '123456789');
        //$this->assertEquals($payload[0]['code'], '500');
        //$this->assertEquals($payload[0]['message'], 'There was a database error when attempting to insert the record.');

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $response->getStatus());
        $this->assertEquals(count($payload[0]), 3);
        $this->assertEquals($payload[0], $this->mockFailedDuplicateCheckReturn());

    }


    /*
     *	Missing PIDM Required Attributes
     *	Tests the Post of a Single PIDM where the number trying to insert is missing a PIDM
     *  required attribute
     *	Expected Return: 200 OK Response with a payload consisting of 4 elements that
                         matches the array in the mockFailedDuplicateCheckReturn method.
     */
    public function testErrorMissingPIDMPost()
    {

        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();

        $request->method('getData')
            ->will($this->returnCallback(array($this, 'mockPostMissingPIDMBody')));
        $this->address->setRequest($request);

        //define what we want isAuthorized method to return
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'mockAuthorizedUser')));
        $this->user->method('getUsername')
            ->will($this->returnCallback(array($this, 'mockAuthorizedUsername')));
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockErrorCheckOkSinglePost')));
        $this->dbh->method('perform')
            ->will($this->returnCallback(array($this, 'mockPerform')));

        /*** Call the postAddress and get the payload ***/
        $response = $this->address->postAddress();
        $payload = $response->getPayload();

        /*** Make assertions about the response from the postAddress method ***/
        //$this->assertEquals($payload[0]['pidm'], '123456789');
        //$this->assertEquals($payload[0]['code'], '500');
        //$this->assertEquals($payload[0]['message'], 'There was a database error when attempting to insert the record.');


        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $response->getStatus());
        $this->assertEquals(count($payload[0]), 3);
        $this->assertEquals($payload[0], $this->mockFailedMissingPidmReturn());

    }

    /*
     *	Missing City Required Attributes
     *	Tests the Post of a Single PIDM where the number trying to insert is missing a City
     *  required attribute
     *	Expected Return: 200 OK Response with a payload consisting of 4 elements that
                         matches the array in the mockFailedDuplicateCheckReturn method.
     */
    public function testErrorMissingCityPost()
    {

        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();
        $request->method('getData')
            ->will($this->returnCallback(array($this, 'mockPostMissingCityBody')));
        $this->address->setRequest($request);

        //define what we want isAuthorized method to return
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'mockAuthorizedUser')));
        $this->user->method('getUsername')
            ->will($this->returnCallback(array($this, 'mockAuthorizedUsername')));
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockErrorCheckOkSinglePost')));
        $this->dbh->method('perform')
            ->will($this->returnCallback(array($this, 'mockPerform')));

        /*** Call the postAddress and get the payload ***/
        $response = $this->address->postAddress();
        $payload = $response->getPayload();

        /*** Make assertions about the response from the postAddress method ***/
        //$this->assertEquals($payload[0]['pidm'], '1147436');
        //$this->assertEquals($payload[0]['code'], '500');
        //$this->assertEquals($payload[0]['message'], 'There was a database error when attempting to insert the record.');

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $response->getStatus());
        $this->assertEquals(count($payload[0]), 3);
        $this->assertEquals($payload[0], $this->mockFailedRequiredAttributeReturn());

    }

    /*
     *	Missing Address Type Required Attributes
     *	Tests the Post of a Single PIDM where the number trying to insert is missing a
     *  Address Type required attribute
     *	Expected Return: 200 OK Response with a payload consisting of 4 elements that
                         matches the array in the mockFailedDuplicateCheckReturn method.
     */
    public function testErrorMissingAddressTypePost()
    {

        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();
        $request->method('getData')
            ->will($this->returnCallback(array($this, 'mockPostMissingAddressTypeBody')));
        $this->address->setRequest($request);

        //define what we want isAuthorized method to return
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'mockAuthorizedUser')));
        $this->user->method('getUsername')
            ->will($this->returnCallback(array($this, 'mockAuthorizedUsername')));
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockErrorCheckOkSinglePost')));
        $this->dbh->method('perform')
            ->will($this->returnCallback(array($this, 'mockPerform')));

        /*** Call the postAddress and get the payload ***/
        $response = $this->address->postAddress();
        $payload = $response->getPayload();

        /*** Make assertions about the response from the postAddress method ***/
        //$this->assertEquals($payload[0]['pidm'], '1147436');
        //$this->assertEquals($payload[0]['code'], '500');
        //$this->assertEquals($payload[0]['message'], 'There was a database error when attempting to insert the record.');

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $response->getStatus());
        $this->assertEquals(count($payload[0]), 3);
        $this->assertEquals($payload[0], $this->mockFailedRequiredAttributeReturn());

    }

    /*
     *	Missing Data Origin Required Attributes
     *	Tests the Post of a Single PIDM where the number trying to insert is missing a
     *  Data Origin required attribute
     *	Expected Return: 200 OK Response with a payload consisting of 4 elements that
                         matches the array in the mockFailedDuplicateCheckReturn method.
     */
    public function testErrorMissingDataOriginPost()
    {

        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();

        $request->method('getData')
            ->will($this->returnCallback(array($this, 'mockPostMissingDataOriginBody')));
        $this->address->setRequest($request);

        //define what we want isAuthorized method to return
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'mockAuthorizedUser')));
        $this->user->method('getUsername')
            ->will($this->returnCallback(array($this, 'mockAuthorizedUsername')));
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockErrorCheckOkSinglePost')));
        $this->dbh->method('perform')
            ->will($this->returnCallback(array($this, 'mockPerform')));

        /*** Call the postAddress and get the payload ***/
        $response = $this->address->postAddress();
        $payload = $response->getPayload();

        /*** Make assertions about the response from the postAddress method ***/
        //$this->assertEquals($payload[0]['pidm'], '1147436');
        //$this->assertEquals($payload[0]['code'], '500');
        //$this->assertEquals($payload[0]['message'], 'There was a database error when attempting to insert the record.');

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $response->getStatus());
        $this->assertEquals(count($payload[0]), 3);
        $this->assertEquals($payload[0], $this->mockFailedRequiredAttributeReturn());

    }

    /*
     *	Missing User ID Required Attributes
     *	Tests the Post of a Single PIDM where the number trying to insert is missing a
     *  User ID required attribute
     *	Expected Return: 200 OK Response with a payload consisting of 4 elements that
                         matches the array in the mockFailedDuplicateCheckReturn method.
     */
    public function testErrorMissingUserIDPost()
    {

        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();

        $request->method('getData')
            ->will($this->returnCallback(array($this, 'mockPostMissingUserIDBody')));
        $this->address->setRequest($request);

        //define what we want isAuthorized method to return
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'mockAuthorizedUser')));
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockErrorCheckOkSinglePost')));
        $this->dbh->method('perform')
            ->will($this->returnCallback(array($this, 'mockPerform')));

        /*** Call the postAddress and get the payload ***/
        $response = $this->address->postAddress();
        $payload = $response->getPayload();

        /*** Make assertions about the response from the postAddress method ***/
        //$this->assertEquals($payload[0]['pidm'], '1147436');
        //$this->assertEquals($payload[0]['code'], '500');
        //$this->assertEquals($payload[0]['message'], 'There was a database error when attempting to insert the record.');

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $response->getStatus());
        $this->assertEquals(count($payload[0]), 3);
        $this->assertEquals($payload[0], $this->mockFailedRequiredAttributeReturn());

    }

    /*
     *	Empty PIDM Required Attributes
     *	Tests the Post of a Single PIDM where the number trying to insert is empty PIDM
     *  required attribute
     *	Expected Return: 200 OK Response with a payload consisting of 4 elements that
                         matches the array in the mockFailedDuplicateCheckReturn method.
     */
    public function testErrorEmptyPIDMPost()
    {

        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();

        $request->method('getData')
            ->will($this->returnCallback(array($this, 'mockPostEmptyPIDMBody')));
        $this->address->setRequest($request);

        //define what we want isAuthorized method to return
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'mockAuthorizedUser')));
        $this->user->method('getUsername')
            ->will($this->returnCallback(array($this, 'mockAuthorizedUsername')));
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockErrorCheckOkSinglePost')));
        $this->dbh->method('perform')
            ->will($this->returnCallback(array($this, 'mockPerform')));

        /*** Call the postAddress and get the payload ***/
        $response = $this->address->postAddress();
        $payload = $response->getPayload();

        /*** Make assertions about the response from the postAddress method ***/
        //$this->assertEquals($payload[0]['pidm'], '1147436');
        //$this->assertEquals($payload[0]['code'], '500');
        //$this->assertEquals($payload[0]['message'], 'There was a database error when attempting to insert the record.');

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $response->getStatus());
        $this->assertEquals(count($payload[0]), 3);
        $this->assertEquals($payload[0], $this->mockFailedMissingPidmReturn
        ());

    }

    /*
     *	Empty City Required Attributes
     *	Tests the Post of a Single PIDM where the number trying to insert is empty City
     *  required attribute
     *	Expected Return: 200 OK Response with a payload consisting of 4 elements that
                         matches the array in the mockFailedDuplicateCheckReturn method.
     */
    public function testErrorEmptyCityPost()
    {

        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();

        $request->method('getData')
            ->will($this->returnCallback(array($this, 'mockPostEmptyCityBody')));
        $this->address->setRequest($request);

        //define what we want isAuthorized method to return
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'mockAuthorizedUser')));
        $this->user->method('getUsername')
            ->will($this->returnCallback(array($this, 'mockAuthorizedUsername')));
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockErrorCheckOkSinglePost')));
        $this->dbh->method('perform')
            ->will($this->returnCallback(array($this, 'mockPerform')));

        /*** Call the postAddress and get the payload ***/
        $response = $this->address->postAddress();
        $payload = $response->getPayload();

        /*** Make assertions about the response from the postAddress method ***/
        //$this->assertEquals($payload[0]['pidm'], '1147436');
        //$this->assertEquals($payload[0]['code'], '500');
        //$this->assertEquals($payload[0]['message'], 'There was a database error when attempting to insert the record.');

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $response->getStatus());
        $this->assertEquals(count($payload[0]), 3);
        $this->assertEquals($payload[0], $this->mockFailedRequiredAttributeReturn());

    }

    /*
     *	Empty Address Type Required Attributes
     *	Tests the Post of a Single PIDM where the number trying to insert is empty
     *  Address Type required attribute
     *	Expected Return: 200 OK Response with a payload consisting of 4 elements that
                         matches the array in the mockFailedDuplicateCheckReturn method.
     */
    public function testErrorEmptyAddressTypePost()
    {

        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();

        $request->method('getData')
            ->will($this->returnCallback(array($this, 'mockPostEmptyAddressTypeBody')));
        $this->address->setRequest($request);

        //define what we want isAuthorized method to return
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'mockAuthorizedUser')));
        $this->user->method('getUsername')
            ->will($this->returnCallback(array($this, 'mockAuthorizedUsername')));
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockErrorCheckOkSinglePost')));
        $this->dbh->method('perform')
            ->will($this->returnCallback(array($this, 'mockPerform')));

        /*** Call the postAddress and get the payload ***/
        $response = $this->address->postAddress();
        $payload = $response->getPayload();

        /*** Make assertions about the response from the postAddress method ***/
        //$this->assertEquals($payload[0]['pidm'], '1147436');
        //$this->assertEquals($payload[0]['code'], '500');
        //$this->assertEquals($payload[0]['message'], 'There was a database error when attempting to insert the record.');

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $response->getStatus());
        $this->assertEquals(count($payload[0]), 3);
        $this->assertEquals($payload[0], $this->mockFailedRequiredAttributeReturn());

    }

    /*
     *	Empty Data Origin Required Attributes
     *	Tests the Post of a Single PIDM where the number trying to insert is empty a
     *  Data Origin required attribute
     *	Expected Return: 200 OK Response with a payload consisting of 4 elements that
                         matches the array in the mockFailedDuplicateCheckReturn method.
     */
    public function testErrorEmptyDataOriginPost()
    {
        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();

        $request->method('getData')
            ->will($this->returnCallback(array($this, 'mockPostEmptyDataOriginBody')));
        $this->address->setRequest($request);

        //define what we want isAuthorized method to return
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'mockAuthorizedUser')));
        $this->user->method('getUsername')
            ->will($this->returnCallback(array($this, 'mockAuthorizedUsername')));
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockErrorCheckOkSinglePost')));
        $this->dbh->method('perform')
            ->will($this->returnCallback(array($this, 'mockPerform')));

        /*** Call the postAddress and get the payload ***/
        $response = $this->address->postAddress();
        $payload = $response->getPayload();

        /*** Make assertions about the response from the postAddress method ***/
        //$this->assertEquals($payload[0]['pidm'], '1147436');
        //$this->assertEquals($payload[0]['code'], '500');
        //$this->assertEquals($payload[0]['message'], 'There was a database error when attempting to insert the record.');

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $response->getStatus());
        $this->assertEquals(count($payload[0]), 3);
        $this->assertEquals($payload[0], $this->mockFailedRequiredAttributeReturn());

    }

    /*
     *	Empty User ID Required Attributes
     *	Tests the Post of a Single PIDM where the number trying to insert is empty a
     *  User ID required attribute
     *	Expected Return: 200 OK Response with a payload consisting of 4 elements that
                         matches the array in the mockFailedDuplicateCheckReturn method.
     */
    public function testErrorEmptyUserIDPost()
    {

        //set up the mock request:
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();

        $request->method('getData')
            ->will($this->returnCallback(array($this, 'mockPostEmptyUserIDBody')));


        //define what we want isAuthorized method to return
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'mockAuthorizedUser')));
        $this->user->method('getUsername')
            ->will($this->returnCallback(array($this, 'mockAuthorizedUsername')));
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockErrorCheckOkSinglePost')));
        $this->dbh->method('perform')
            ->will($this->returnCallback(array($this, 'mockPerform')));

        /*** Call the postAddress and get the payload ***/
        $this->address->setRequest($request);
        $response = $this->address->postAddress();
        $payload = $response->getPayload();

        /*** Make assertions about the response from the postAddress method ***/
        //$this->assertEquals($payload[0]['pidm'], '1147436');
        //$this->assertEquals($payload[0]['code'], '500');
        //$this->assertEquals($payload[0]['message'], 'There was a database error when attempting to insert the record.');

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $response->getStatus());
        $this->assertEquals(count($payload[0]), 3);
        $this->assertEquals($payload[0], $this->mockFailedRequiredAttributeReturn());

    }



    /*************************/
    /**Start of Mock Methods**/
    /*************************/

    /*
     *	Mock options array for a requests.
     */
    public function mockOptions()
    {
        $optionsArray = array('pidm' => array('123456789'));
        return $optionsArray;
    }

    /*
     *	Method which mocks the username of a user who has access to this service.
     */
    public function mockAuthorizedUsername()
    {
        return "tester";
    }

    /*
     *	Method which mocks the authorization of a user who has access to this service.
     */
    public function mockAuthorizedUser()
    {
        return true;
    }

    /*
    *	Method that mocks the authorization of a user who does not have access to this
    *	service
    */
    public function mockNotAuthorizedUser()
    {
        return false;
    }

    /*
    *	Method which mocks the POST Body for a valid POST request.
    */
    public function mockPostBody()
    {
        $payload = array(
            'address0' => array(
                'pidm' => '123456789',
                'addressType' => 'A1',
                'streetLine1' => '123 No Where Dr',
                'streetLine2' => 'Apt 1',
                'streetLine3' => '',
                'city' => 'NoWhere',
                'stateCode' => 'NA',
                'zip' => '12345',
                "dataOrigin" => "PHP Unit Test",
                "userId" => "tester",
                "sequenceNumber" => ""
            )

        );
        return $payload;
    }


    /*
     *	Method which mocks the POST Body Empty PIDM Attirbute
     */
    public function mockPostEmptyPIDMBody()
    {
        $payload = array(
            'address0' => array(
                'pidm' => '',
                'addressType' => 'A1',
                'streetLine1' => '123 No Where Dr',
                'streetLine2' => 'Apt 1',
                'streetLine3' => '',
                'city' => 'NoWhere',
                'stateCode' => 'NA',
                'zip' => '12345',
                'dataOrigin' => 'PHP Unit Test',
                'userId' => 'tester',
                'sequenceNumber' => ''
            )

        );
        return $payload;
    }


    /*
     *	Method which mocks the POST Body Empty City Attribute
     */
    public function mockPostEmptyCityBody()
    {
        $payload = array(
            'address0' => array(
                'pidm' => '123456789',
                'addressType' => 'A1',
                'streetLine1' => '123 No Where Dr',
                'streetLine2' => 'Apt 1',
                'streetLine3' => '',
                'city' => '',
                'stateCode' => 'NA',
                'zip' => '12345',
                'dataOrigin' => 'PHP Unit Test',
                'userId' => 'tester',
                'sequenceNumber' => ''
            )

        );
        return $payload;
    }

    /*
     *	Method which mocks the POST Body Empty Address Type Attirbute
     */
    public function mockPostEmptyAddressTypeBody()
    {
        $payload = array(
            'address0' => array(
                'pidm' => '123456789',
                'addressType' => '',
                'streetLine1' => '123 No Where Dr',
                'streetLine2' => 'Apt 1',
                'streetLine3' => '',
                'city' => 'NoWhere',
                'stateCode' => 'NA',
                'zip' => '12345',
                'dataOrigin' => 'PHP Unit Test',
                'userId' => 'tester',
                'sequenceNumber' => ''
            )

        );
        return $payload;
    }


    /*
     *	Method which mocks the POST Body Empty User ID Attirbute
     */
    public function mockPostEmptyUserIDBody()
    {
        $payload = array(
            'address0' => array(
                'pidm' => '123456789',
                'addressType' => 'A1',
                'streetLine1' => '123 No Where Dr',
                'streetLine2' => 'Apt 1',
                'streetLine3' => '',
                'city' => 'NoWhere',
                'stateCode' => 'NA',
                'zip' => '12345',
                'dataOrigin' => 'PHP Unit Test',
                'userId' => '',
                'sequenceNumber' => ''
            )

        );
        return $payload;
    }

    /*
     *	Method which mocks the POST Body Empty Data Origin Attirbute
     */
    public function mockPostEmptyDataOriginBody()
    {
        $payload = array(
            'address0' => array(
                'pidm' => '123456789',
                'addressType' => 'A1',
                'streetLine1' => '123 No Where Dr',
                'streetLine2' => 'Apt 1',
                'streetLine3' => '',
                'city' => 'NoWhere',
                'stateCode' => 'NA',
                'zip' => '12345',
                'dataOrigin' => '',
                'userId' => 'tester',
                'sequenceNumber' => ''
            )

        );
        return $payload;
    }


    /*
     *	Method which mocks the POST Body Missing PIDM Attirbute
     */
    public function mockPostMissingPIDMBody()
    {
        $payload = array(
            'address0' => array(
                'addressType' => 'A1',
                'streetLine1' => '123 No Where Dr',
                'streetLine2' => 'Apt 1',
                'streetLine3' => '',
                'city' => 'NoWhere',
                'stateCode' => 'NA',
                'zip' => '12345',
                'dataOrigin' => 'PHP Unit Test',
                'userId' => 'tester',
                'sequenceNumber' => ''
            )

        );
        return $payload;
    }


    /*
     *	Method which mocks the POST Body Missing City Attirbute
     */
    public function mockPostMissingCityBody()
    {
        $payload = array(
            'address0' => array(
                'pidm' => '123456789',
                'addressType' => 'A1',
                'streetLine1' => '123 No Where Dr',
                'streetLine2' => 'Apt 1',
                'streetLine3' => '',
                'stateCode' => 'NA',
                'zip' => '12345',
                'dataOrigin' => 'PHP Unit Test',
                'userId' => 'tester',
                'sequenceNumber' => ''
            )

        );
        return $payload;
    }

    /*
     *	Method which mocks the POST Body Missing Address Type Attirbute
     */
    public function mockPostMissingAddressTypeBody()
    {
        $payload = array(
            'address0' => array(
                'pidm' => '123456789',
                'streetLine1' => '123 No Where Dr',
                'streetLine2' => 'Apt 1',
                'streetLine3' => '',
                'city' => 'NoWhere',
                'stateCode' => 'NA',
                'zip' => '12345',
                'dataOrigin' => 'PHP Unit Test',
                'userId' => 'tester',
                'sequenceNumber' => ''
            )

        );
        return $payload;
    }


    /*
     *	Method which mocks the POST Body Missing User ID Attirbute
     */
    public function mockPostMissingUserIDBody()
    {
        $payload = array(
            'address0' => array(
                'pidm' => '123456789',
                'addressType' => 'A1',
                'streetLine1' => '123 No Where Dr',
                'streetLine2' => 'Apt 1',
                'streetLine3' => '',
                'city' => 'NoWhere',
                'stateCode' => 'NA',
                'zip' => '12345',
                'dataOrigin' => 'PHP Unit Test',
                'sequenceNumber' => ''
            )

        );
        return $payload;
    }

    /*
     *	Method which mocks the POST Body Missing Data Origin Attirbute
     */
    public function mockPostMissingDataOriginBody()
    {
        $payload = array(
            'address0' => array(
                'pidm' => '123456789',
                'addressType' => 'A1',
                'streetLine1' => '123 No Where Dr',
                'streetLine2' => 'Apt 1',
                'streetLine3' => '',
                'city' => 'NoWhere',
                'stateCode' => 'NA',
                'zip' => '12345',
                'userId' => 'tester',
                'sequenceNumber' => ''
            )

        );
        return $payload;
    }


    /*
    *	Method which returns a mocked out version of the error check which is done prior
    *	to initiating the Insert and it passes.
    */
    public function mockErrorCheckOkSinglePost()
    {
        $this->queryallRecords = array(
            array(
                'spraddr_pidm' => '123456789',
                'spraddr_atyp_code' => 'A1',
                'spraddr_seqno' => '1',
                'spraddr_street_line1' => '555 No Where Dr',
                'spraddr_street_line2' => 'Apt 1',
                'spraddr_street_line3' => '',
                'spraddr_street_line4' => '',
                'spraddr_city' => 'NoWhere',
                'spraddr_stat_code' => 'NA',
                'spraddr_zip' => '12345',
                'spbpers_confid_ind' => ''

            ),
            array(
                'spraddr_pidm' => '123456789',
                'spraddr_atyp_code' => 'MA',
                'spraddr_seqno' => '2',
                'spraddr_street_line1' => '456 Some Where St',
                'spraddr_street_line2' => '',
                'spraddr_street_line3' => '',
                'spraddr_street_line4' => '',
                'spraddr_city' => 'Some Where',
                'spraddr_stat_code' => 'NA',
                'spraddr_zip' => '67890',
                'spbpers_confid_ind' => ''

            )
        );
        return $this->queryallRecords;
    }


    /*
     *	Method which returns a mocked out version of the error check which is done prior
     *	to initiating the Insert and it doesn't pass due to Confidentiality Flag.
     */
    public function mockErrorCheckConfidentialSinglePost()
    {
        $this->queryallRecords = array(
            array(
                'spraddr_pidm' => '123456789',
                'spraddr_atyp_code' => 'A1',
                'spraddr_seqno' => '1',
                'spraddr_street_line1' => '555 No Where Dr',
                'spraddr_street_line2' => 'Apt 1',
                'spraddr_street_line3' => '',
                'spraddr_street_line4' => '',
                'spraddr_city' => 'NoWhere',
                'spraddr_stat_code' => 'NA',
                'spraddr_zip' => '12345',
                'spbpers_confid_ind' => 'Y'

            ),
            array(
                'spraddr_pidm' => '123456789',
                'spraddr_atyp_code' => 'MA',
                'spraddr_seqno' => '2',
                'spraddr_street_line1' => '456 Some Where St',
                'spraddr_street_line2' => '',
                'spraddr_street_line3' => '',
                'spraddr_street_line4' => '',
                'spraddr_city' => 'Some Where',
                'spraddr_stat_code' => 'NA',
                'spraddr_zip' => '67890',
                'spbpers_confid_ind' => 'Y'

            )
        );
        return $this->queryallRecords;
    }

    /*
     *	Method which returns a mocked out version of the error check which is done prior
     *	to initiating the Insert and it doesn't pass due to duplicate number.
     */
    public function mockErrorCheckDuplicateSinglePost()
    {
        $this->queryallRecords = array(
            array(
                'spraddr_pidm' => '123456789',
                'spraddr_atyp_code' => 'A1',
                'spraddr_seqno' => '1',
                'spraddr_street_line1' => '123 No Where Dr',
                'spraddr_street_line2' => 'Apt 1',
                'spraddr_street_line3' => '',
                'spraddr_street_line4' => '',
                'spraddr_city' => 'NoWhere',
                'spraddr_stat_code' => 'NA',
                'spraddr_zip' => '12345',
                'spbpers_confid_ind' => ''
            ),
            array(
                'spraddr_pidm' => '123456789',
                'spraddr_atyp_code' => 'MA',
                'spraddr_seqno' => '2',
                'spraddr_street_line1' => '456 Some Where St',
                'spraddr_street_line2' => '',
                'spraddr_street_line3' => '',
                'spraddr_street_line4' => '',
                'spraddr_city' => 'Some Where',
                'spraddr_stat_code' => 'NA',
                'spraddr_zip' => '67890',
                'spbpers_confid_ind' => ''
            )
        );

        return $this->queryallRecords;
    }

    /*
    *	Mocked version of a succcessful Perform call.
    */
    public function mockPerform()
    {
        return true;
    }

    /*
    *	Mocked version of the expected return of a Single PIDM POST call.
    */
    public function mockSuccessfulSinglePostReturn()
    {
        $expectedReturn = array(
            "pidm" => "123456789",
            "code" => 201,
            "message" => "",
        );
        return $expectedReturn;
    }


    /*
     *	Mocked version of the expected return of a Failed Confidentiality Check.
     */

    public function mockFailedConfidentialityCheckReturn()
    {
        $expectedReturn = array(
            "pidm" => "123456789",
            "code" => 500,
            "message" => "Cannot be updated/Inserted, data protected",
        );
        return $expectedReturn;
    }

    /*
     *	Mocked version of the expected return of a Failed Duplicate Check.
     */

    public function mockFailedDuplicateCheckReturn()
    {
        $expectedReturn = array(
            "pidm" => "123456789",
            "code" => 500,
            "message" => "Duplicate entry, check address 123 No Where Dr, Apt 1, , NoWhere, NA 12345 type A1");
        return $expectedReturn;
    }

    /*
     *	Mocked version of the expected return of a Failed Duplicate Check.
     */

    public function mockFailedRequiredAttributeReturn()
    {
        $expectedReturn = array(
            "pidm" => "123456789",
            "code" => 500,
            "message" => "Missing Required Field.",
        );
        return $expectedReturn;
    }

    /*
     *	Mocked version of the expected return of a Failed Duplicate Check.
     */

    public function mockFailedMissingPidmReturn()
    {
        $expectedReturn = array(
            "pidm" => "",
            "code" => 500,
            "message" => "Missing Required Field.",
        );
        return $expectedReturn;
    }

}
