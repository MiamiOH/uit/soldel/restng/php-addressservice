<?php

namespace MiamiOH\RestngAddressService\Tests\Unit;

class addressv2CreateTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $address;
    private $cleanAddress;
    private $updateAddress;

    private $apiUser;
    private $dbh;
    private $sth;

    private $bindPlaceHolder = '';
    private $boundValues = [];
    private $mockApiUsername = '';

    private $mockClnAddrRes = [];

    protected function setUp()
    {

        $this->bindPlaceHolder = '';
        $this->boundValues = [];
        $this->mockApiUsername = '';
        $this->mockClnAddrRes = [];

        $this->apiUser = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            ->setMethods(array('getUsername'))
            ->getMock();

        $this->sth = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\STH')
            ->setMethods(array('execute', 'bind_by_name'))
            ->getMock();

        $this->sth->method('bind_by_name')
            ->with($this->callback(array($this, 'bind_by_nameWithName')), $this->callback(array($this, 'bind_by_nameWithValue')))
            ->willReturn(true);

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('auto_commit', 'prepare', 'commit', 'rollback', 'queryall_array'))
            ->getMock();

        $this->dbh->method('prepare')->willReturn($this->sth);

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->address = new \MiamiOH\RestngAddressService\Services\Addressv2();

        $this->address->setDatabase($db);

        $this->address->setApiUser($this->apiUser);


        $this->cleanAddress = $this->getMockBuilder('\MiamiOH\RestngAddressService\Services\CleanAddress')
            ->setMethods(array('CleanAddressVerify', 'getCleanAddressResponse'))
            ->getMock();

        $this->address->SetCleanAddress($this->cleanAddress);

        $this->updateAddress = $this->getMockBuilder('\MiamiOH\RestngAddressService\Services\UpdateAddressv2')
            ->setMethods(array('setDBH', 'endLocalAddress'))
            ->getMock();

        $this->address->setUpdateAddressv2($this->updateAddress);

        $this->updateAddress->method('setDBH')
            ->with($dbh = $this->dbh);

        $this->updateAddress->method('endLocalAddress')
            ->with($pidm = 123456, $addressType = 'LO')
            ->willReturn(true);

    }

    public function testAddressv2CreateValidAddress()
    {

        $this->mockApiUsername = 'MUWS_GEN';

        $model = [
            'pidm' => 123456,
            'addressType' => 'LO',
            'streetLine1' => '1st street',
            'streetLine2' => 'APT 5',
            'streetLine3' => null,
            'city' => 'Oxford',
            'state' => 'OH',
            'postalCode' => '45056',
            'sequenceNumber' => null,
            'status' => null,
            'id' => 'ABC',
            'addressSourceCode' => null
        ];

        $verifiedAddress = [
            'pidm' => 123456,
            'addressType' => 'LO',
            'streetLine1' => '1st street',
            'streetLine2' => 'APT 5',
            'streetLine3' => null,
            'city' => 'Oxford',
            'state' => 'OH',
            'postalCode' => '45056',
            'sequenceNumber' => null,
            'status' => null,
            'id' => 'ABC',
            'addressSourceCode' => null
        ];

        $this->mockClnAddrRes = [
            'errorCode' => '',
            'errorString' => '',
            'suggestionList' => '',
        ];

        $this->records = [
            [
                'spraddr_pidm' => '123456',
                'spraddr_atyp_code' => 'LO',
                'spraddr_status_ind' => null,
                'spraddr_from_date' => '2016-08-03',
                'spraddr_to_date' => null,
                'spraddr_street_line1' => '1st street',
                'spraddr_street_line2' => 'APT 5',
                'spraddr_street_line3' => null,
                'spraddr_city' => 'Oxford',
                'spraddr_stat_code' => 'OH',
                'spraddr_zip' => '45056',
                'spraddr_natn_code' => '',
                'stvnatn_nation' => '',
                'spraddr_seqno' => '1',
                'row_id'=>'ABC',
                'spraddr_asrc_code' => null
            ],
        ];


        $this->apiUser->expects($this->once())->method('getUsername')
            ->will($this->returnCallback(array($this, 'getUsernameMock')));

        $this->cleanAddress->method('CleanAddressVerify')
            ->with($model)
            ->willReturn($verifiedAddress);

        $this->cleanAddress->method('getCleanAddressResponse')
            ->will($this->returnCallback(array($this, 'mockClnAddrResponse')));

        $this->cleanAddress->method('getCleanAddressResponse')
            ->will($this->returnCallback(array($this, 'mockClnAddrResponse')));

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $result = $this->address->create($model);

        $record = $this->records[0];


        $this->assertEquals($model['pidm'], $this->boundValues[':P_PIDM'], 'PIDM is bound to correct PIDM');
        $this->assertEquals($model['addressType'], $this->boundValues[':P_ATYP_CODE'], 'Address type is bound to correct type');
        $this->assertEquals($this->mockApiUsername, $this->boundValues[':P_USER'], 'Active user is bound to correct value');
        $this->assertEquals($this->mockClnAddrRes['errorCode'], '', 'Clean Address passed');

        $this->assertTrue(strpos($this->queryString, 'FROM SPRADDR') !== false, 'Query contains from spraddr');
        $this->assertTrue(strpos($this->queryString, 'WHERE') !== false, 'Query contains WHERE');
        $this->assertEquals($record['spraddr_from_date'], $result['fromDate']);
        $this->assertEquals($record['spraddr_seqno'], $result['sequenceNumber']);

    }

    /**
     * @expectedException \MiamiOH\RestngAddressService\Exceptions\CleanAddressFailed
     * @expectedExceptionMessage Invalid Address provided.Please look at the Suggestion List
     */
    public function testAddressv2CreateInvalidAddress()
    {

        $this->mockApiUsername = 'MUWS_GEN';

        $skipValidation = 'no';

        $model = [
            'pidm' => 123456,
            'addressType' => 'LO',
            'streetLine1' => '1st street',
            'streetLine2' => 'APT 5',
            'streetLine3' => null,
            'city' => 'Oxford',
            'state' => 'OH',
            'postalCode' => '00000',
            'sequenceNumber' => null,
            'status' => null,
        ];

        $verifiedAddress = [
            'pidm' => 123456,
            'addressType' => 'LO',
            'streetLine1' => '1st street',
            'streetLine2' => 'APT 5',
            'streetLine3' => null,
            'city' => 'Oxford',
            'state' => 'OH',
            'postalCode' => '00000',
            'sequenceNumber' => null,
            'status' => null,
        ];

        $this->mockClnAddrRes = [
            'errorCode' => 'Z',
            'errorString' => 'ZIP Code Error',
            'suggestionList' => '',
        ];


        /* This test does not require queryall mock and getusername mock as it would fail with cleanaddress check. */

        $this->cleanAddress->method('CleanAddressVerify')
            ->with($model)
            ->willReturn($verifiedAddress);

        $this->cleanAddress->method('getCleanAddressResponse')
            ->will($this->returnCallback(array($this, 'mockClnAddrResponse')));

        $this->cleanAddress->method('getCleanAddressResponse')
            ->will($this->returnCallback(array($this, 'mockClnAddrResponse')));


        $result = $this->address->filterSkipValidation($skipValidation)->create($model);


        $this->assertEquals($model['pidm'], $this->boundValues[':P_PIDM'], 'PIDM is bound to correct PIDM');
        $this->assertEquals($model['addressType'], $this->boundValues[':P_ATYP_CODE'], 'Address type is bound to correct type');
        $this->assertEquals($this->mockApiUsername, $this->boundValues[':P_USER'], 'Active user is bound to correct value');
        $this->assertEquals($this->mockClnAddrRes['errorCode'], 'Z', 'Clean Address failed');
        $this->assertEquals($this->mockClnAddrRes['suggestionList'], '', 'Clean Address failed with no Suggestion');

    }

    /**
     * @expectedException \MiamiOH\RestngAddressService\Exceptions\CleanAddressFailed
     * @expectedExceptionMessage Invalid Address provided.Please look at the Suggestion List
     */
    public function testAddressv2CreateFailedwithSuggestion()
    {

        $this->mockApiUsername = 'MUWS_GEN';

        $skipValidation = 'no';

        $model = [
            'pidm' => 123456,
            'addressType' => 'LO',
            'streetLine1' => '123 XYZ',
            'streetLine2' => 'APT 5',
            'streetLine3' => null,
            'city' => 'Oxford',
            'state' => 'OH',
            'postalCode' => '45056',
            'sequenceNumber' => null,
            'status' => null,
        ];

        $verifiedAddress = [
            'pidm' => 123456,
            'addressType' => 'LO',
            'streetLine1' => '123 XYZ',
            'streetLine2' => 'APT 5',
            'streetLine3' => null,
            'city' => 'Oxford',
            'state' => 'OH',
            'postalCode' => '45056',
            'sequenceNumber' => null,
            'status' => null,
        ];

        $this->mockClnAddrRes = [
            'errorCode' => 'M',
            'errorString' => 'Multiple Matches',
            'suggestionList' => '123 XYZ Street,123 XYZ Blvd',
        ];

        /* This test does not require queryall mock and getusername mock as it would fail with cleanaddress check. */


        $this->cleanAddress->method('CleanAddressVerify')
            ->with($model)
            ->willReturn($verifiedAddress);

        $this->cleanAddress->method('getCleanAddressResponse')
            ->will($this->returnCallback(array($this, 'mockClnAddrResponse')));

        $this->cleanAddress->method('getCleanAddressResponse')
            ->will($this->returnCallback(array($this, 'mockClnAddrResponse')));


        $result = $this->address->filterSkipValidation($skipValidation)->create($model);


        $this->assertEquals($model['pidm'], $this->boundValues[':P_PIDM'], 'PIDM is bound to correct PIDM');
        $this->assertEquals($model['addressType'], $this->boundValues[':P_ATYP_CODE'], 'Address type is bound to correct type');
        $this->assertEquals($this->mockApiUsername, $this->boundValues[':P_USER'], 'Active user is bound to correct value');
        $this->assertEquals($this->mockClnAddrRes['errorCode'], 'M', 'Clean Address failed');
        $this->assertEquals($this->mockClnAddrRes['suggestionList'], '123 XYZ Street,123 XYZ Blvd', 'Clean Address failed with Suggestion');

    }

    public function bind_by_nameWithName($subject)
    {
        $this->bindPlaceHolder = $subject;
        return true;
    }

    public function bind_by_nameWithValue($subject)
    {
        $this->boundValues[$this->bindPlaceHolder] = $subject;
        $this->bindPlaceHolder = '';
        return true;
    }

    public function getUsernameMock()
    {
        return $this->mockApiUsername;
    }

    public function mockClnAddrResponse()
    {
        return $this->mockClnAddrRes;
    }

    public function queryall_arrayWithQuery($subject)
    {
        $this->queryString = $subject;
        return true;
    }

    public function queryall_arrayWithParams($subject)
    {
        $this->queryParams = $subject;
        return true;
    }

    public function queryall_arrayMock()
    {
        return $this->records;
    }
}
