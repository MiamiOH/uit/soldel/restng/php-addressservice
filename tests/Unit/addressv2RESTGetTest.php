<?php

namespace MiamiOH\RestngAddressService\Tests\Unit;


use MiamiOH\RESTng\App;

class addressv2RESTGetTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $addressREST;
    private $api;
    private $address;
    private $request;
    private $addressData = [];
    private $requestResourceParam = '';
    private $requestResourceParamMocks = [];
    private $mockReadResponse = [];
    private $readPidm = '';
    private $filterType = '';
    private $filterStatus = '';

    protected function setUp()
    {

        $this->addressData = [];
        $this->requestResourceParam = '';
        $this->requestResourceParamMocks = [];
        $this->mockReadResponse = [];
        $this->readPidm = '';
        $this->filterType = '';
        $this->filterStatus = '';

        //set up the mock api:
        $this->api = $this->createMock(App::class);


        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->bannerId = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
            ->setMethods(array('getPidm'))
            ->getMock();

        $bannerUtil = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerUtil')
            ->setMethods(array('getId'))
            ->getMock();

        $bannerUtil->method('getId')->willReturn($this->bannerId);

        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $this->request->expects($this->once())->method('getResourceParam')
            ->with($this->callback(array($this, 'getResourceParamWith')))
            ->will($this->returnCallback(array($this, 'getResourceParamMock')));

        $this->address = $this->getMockBuilder('\MiamiOH\RestngAddressService\Services\Addressv2')
            ->setMethods(array('read', 'filterType', 'filterStatus'))
            ->getMock();

        $this->address->method('read')
            ->with($this->callback(array($this, 'readWith')))
            ->will($this->returnCallback(array($this, 'readMock')));

        $this->address->method('filterType')
            ->with($this->callback(array($this, 'filterTypeWith')))
            ->will($this->returnSelf());

        $this->address->method('filterStatus')
            ->with($this->callback(array($this, 'filterStatusWith')))
            ->will($this->returnSelf());

        //set up the service with the mocked out resources:
        $this->addressREST = new \MiamiOH\RestngAddressService\Services\Addressv2REST();
        $this->addressREST->setLogger();
        $this->addressREST->setApp($this->api);
        $this->addressREST->setBannerUtil($bannerUtil);
        $this->addressREST->setAddressv2($this->address);

    }

    public function testGetAddressREST()
    {

        $this->requestResourceParamMocks = [
            'muid' => 'doej',
        ];

        $this->mockReadResponse = [
            'pidm' => 123456,
            'addressType' => 'LO',
            'streetLine1' => '1st street',
            'streetLine2' => 'APT 5',
            'streetLine3' => null,
            'city' => 'Oxford',
            'state' => 'OH',
            'postalCode' => '45056',
            'sequenceNumber' => 1,
            'status' => 'active',
        ];

        $this->addressREST->setRequest($this->request);

        $response = $this->addressREST->getAddressv2();

        $payload = $response->getPayload();

        $this->assertEquals($this->mockReadResponse, $payload);
    }

    public function testGetAddressv2RESTType()
    {

        $this->requestResourceParamMocks = [
            'muid' => 'doej',
        ];

        $this->mockReadResponse = [
            'pidm' => 123456,
            'addressType' => 'LO',
            'streetLine1' => '1st street',
            'streetLine2' => 'APT 5',
            'streetLine3' => null,
            'city' => 'Oxford',
            'state' => 'OH',
            'postalCode' => '45056',
            'sequenceNumber' => 1,
            'status' => 'active',
        ];

        $this->request->method('getOptions')
            ->willReturn(['addressType' => 'LO']);

        $this->addressREST->setRequest($this->request);

        $response = $this->addressREST->getAddressv2();

        $payload = $response->getPayload();

        $this->assertEquals($this->mockReadResponse, $payload);
        $this->assertEquals('LO', $this->filterType);

    }

    public function testGetAddressv2RESTStatus()
    {

        $this->requestResourceParamMocks = [
            'muid' => 'doej',
        ];

        $this->mockReadResponse = [
            'pidm' => 123456,
            'addressType' => 'LO',
            'streetLine1' => '1st street',
            'streetLine2' => 'APT 5',
            'streetLine3' => null,
            'city' => 'Oxford',
            'state' => 'OH',
            'postalCode' => '45056',
            'sequenceNumber' => 1,
            'status' => 'active',
        ];

        $this->request->method('getOptions')
            ->willReturn(['status' => 'active']);

        $this->addressREST->setRequest($this->request);

        $response = $this->addressREST->getAddressv2();

        $payload = $response->getPayload();

        $this->assertEquals($this->mockReadResponse, $payload);
        $this->assertEquals('active', $this->filterStatus);

    }

    public function testGetAddressv2RESTInvalidId()
    {

        $this->bannerId->method('getPidm')
            ->will($this->throwException(new \MiamiOH\RESTng\Service\Extension\BannerIdNotFound("No matches for 'szbuniq_unique_id = DOEJ'")));

        $this->addressREST->setRequest($this->request);

        $response = $this->addressREST->getAddressv2();

        $this->assertEquals(\MiamiOH\RESTng\App::API_NOTFOUND, $response->getStatus());

    }

    public function getResourceParamWith($subject)
    {
        $this->requestResourceParam = $subject;
        return true;
    }

    public function getResourceParamMock()
    {
        if (isset($this->requestResourceParamMocks[$this->requestResourceParam])) {
            return $this->requestResourceParamMocks[$this->requestResourceParam];
        }

        return null;
    }

    public function readWith($subject)
    {
        $this->readPidm = $subject;
        return true;
    }

    public function readMock()
    {
        return $this->mockReadResponse;
    }

    public function filterTypeWith($subject)
    {
        $this->filterType = $subject;
        return true;
    }

    public function filterStatusWith($subject)
    {
        $this->filterStatus = $subject;
        return true;
    }

}