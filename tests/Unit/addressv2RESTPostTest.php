<?php

namespace MiamiOH\RestngAddressService\Tests\Unit;


use MiamiOH\RESTng\App;

class addressv2RESTPostTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $addressREST;

    private $api;
    private $address;

    private $mockModel = [];
    private $createModel = [];
    private $requestResourceParam = '';
    private $requestResourceParamMocks = [];

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {

        $this->mockModel = [];
        $this->createModel = [];
        $this->requestResourceParam = '';
        $this->requestResourceParamMocks = [];

        //set up the mock api:
        $this->api = $this->createMock(App::class);


        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->bannerId = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
            ->setMethods(array('getPidm'))
            ->getMock();

        $bannerUtil = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerUtil')
            ->setMethods(array('getId'))
            ->getMock();

        $bannerUtil->method('getId')->willReturn($this->bannerId);

        $this->address = $this->getMockBuilder('\MiamiOH\RestngAddressService\Services\Addressv2')
            ->setMethods(array('create'))
            ->getMock();

        $this->address->method('create')
            ->with($this->callback(array($this, 'createWith')))
            ->will($this->returnCallback(array($this, 'myCreateMock')));

        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getData', 'getOptions'))
            ->getMock();

        $this->request->expects($this->once())->method('getResourceParam')
            ->with($this->callback(array($this, 'getResourceParamWith')))
            ->will($this->returnCallback(array($this, 'getResourceParamMock')));

        $this->request->expects($this->once())->method('getData')
            ->will($this->returnCallback(array($this, 'getDataMock')));

        //set up the service with the mocked out resources:
        $this->addressREST = new \MiamiOH\RestngAddressService\Services\Addressv2REST();
        $this->addressREST->setLogger();
        $this->addressREST->setApp($this->api);
        $this->addressREST->setAddressv2($this->address);
        $this->addressREST->setBannerUtil($bannerUtil);

    }

    /*************************/
    /**********Tests**********/
    /*************************/

    public function testPostAddressv2Collection()
    {
        $this->mockModel = [
            [
                'pidm' => 123456,
                'addressType' => 'LO',
                'streetLine1' => '1st street',
                'streetLine2' => 'APT 5',
                'streetLine3' => null,
                'city' => 'Oxford',
                'state' => 'OH',
                'postalCode' => '45056',
                'sequenceNumber' => null,
                'status' => null,
            ]
        ];

        $this->bannerId->method('getPidm')->willReturn(123456);

        $this->request->method('getOptions')
            ->willReturn(['skipValidation' => 'no']);

        $this->addressREST->setRequest($this->request);

        $response = $this->addressREST->postAddressv2Collection();

        $payload = $response->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $response->getStatus());
        $this->assertEquals(\MiamiOH\RESTng\App::API_CREATED, $payload[0]['status']);
        $this->assertEquals($this->mockModel[0], $payload[0]['address']);
    }

    public function testPostAddressv2CollectionNotFound()
    {
        $this->bannerId->method('getPidm')
            ->will($this->throwException(new \MiamiOH\RESTng\Service\Extension\BannerIdNotFound("No matches for 'szbuniq_unique_id = JONESL'")));

        $this->addressREST->setRequest($this->request);

        $response = $this->addressREST->postAddressv2Collection();

        $this->assertEquals(\MiamiOH\RESTng\App::API_NOTFOUND, $response->getStatus());
    }

    public function testPostAddressv2CollectionBadRequest()
    {
        $this->mockModel = [
            [
                'pidm' => 345677,
                'addressType' => 'LO',
                'streetLine1' => '1st street',
                'streetLine2' => 'APT 5',
                'streetLine3' => null,
                'city' => 'Oxford',
                'state' => 'OH',
                'postalCode' => '45056',
                'sequenceNumber' => null,
                'status' => null,
            ]
        ];

        $this->bannerId->method('getPidm')->willReturn(123456);

        $this->addressREST->setRequest($this->request);

        $response = $this->addressREST->postAddressv2Collection();

        $payload = $response->getPayload();

        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $response->getStatus());
        $this->assertEquals(\MiamiOH\RESTng\App::API_BADREQUEST, $payload[0]['status']);

    }

    public function createWith($subject)
    {
        $this->createModel = $subject;
        return true;
    }

    public function myCreateMock()
    {
        return $this->createModel;
    }

    public function getDataMock()
    {
        return $this->mockModel;
    }

    public function getResourceParamWith($subject)
    {
        $this->requestResourceParam = $subject;
        return true;
    }

    public function getResourceParamMock()
    {
        if (isset($this->requestResourceParamMocks[$this->requestResourceParam])) {
            return $this->requestResourceParamMocks[$this->requestResourceParam];
        }

        return null;
    }
}
