<?php

namespace MiamiOH\RestngAddressService\Tests\Unit;

class addressv2ReadTest extends \MiamiOH\RESTng\Testing\TestCase
{
    private $address;
    private $dbh;
    private $records = [];
    private $queryString = '';
    private $queryParams = [];

    protected function setUp()
    {

        $this->records = [];
        $this->queryString = '';
        $this->queryParams = [];

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->address = new \MiamiOH\RestngAddressService\Services\Addressv2();

        $this->address->setDatabase($db);

    }

    public function testAddressv2Read()
    {

        $pidm = '110518';

        $this->records = [
            [
                'spraddr_pidm' => '123456',
                'spraddr_atyp_code' => 'LO',
                'spraddr_status_ind' => null,
                'spraddr_from_date' => '2016-01-12',
                'spraddr_to_date' => null,
                'spraddr_street_line1' => '1st street',
                'spraddr_street_line2' => 'APT 5',
                'spraddr_street_line3' => null,
                'spraddr_city' => 'Oxford',
                'spraddr_stat_code' => 'OH',
                'spraddr_zip' => '45056',
                'spraddr_natn_code' => '',
                'stvnatn_nation' => '',
                'spraddr_seqno' => '1',
                'row_id'=>'ABC',
                'spraddr_asrc_code' => null
            ],
            [
                'spraddr_pidm' => '123456',
                'spraddr_atyp_code' => 'RH',
                'spraddr_status_ind' => null,
                'spraddr_from_date' => '2016-01-12',
                'spraddr_to_date' => null,
                'spraddr_street_line1' => '2nd street',
                'spraddr_street_line2' => 'APT 10',
                'spraddr_street_line3' => null,
                'spraddr_city' => 'Oxford',
                'spraddr_stat_code' => 'OH',
                'spraddr_zip' => '45056',
                'spraddr_natn_code' => '',
                'stvnatn_nation' => '',
                'spraddr_seqno' => '1',
                'row_id'=>'ABC',
                'spraddr_asrc_code' => null
            ],
        ];

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $models = $this->address->read($pidm);

        $this->assertTrue(in_array($pidm, $this->queryParams));
        $this->assertTrue(stripos($this->queryString, 'FROM SPRADDR') !== false, 'Query contains from spraddr');
        $this->assertTrue(stripos($this->queryString, 'WHERE') !== false, 'Query contains WHERE');
        $this->assertTrue(stripos($this->queryString, 'ORDER BY') !== false, 'Query contains ORDER BY');

        $this->assertEquals(2, count($models));

        $record = $this->records[0];
        $model = $models[0];

        $this->assertEquals($record['spraddr_pidm'], $model['pidm']);
        $this->assertEquals($record['spraddr_atyp_code'], $model['addressType']);
        $this->assertEquals($record['spraddr_from_date'], $model['fromDate']);
        $this->assertEquals($record['spraddr_seqno'], $model['sequenceNumber']);
        $this->assertEquals('active', $model['status']);
    }

    public function testAddressReadByType()
    {

        $pidm = '123456';
        $types = ['LO', 'MA'];

        $this->records = [
            [
                'spraddr_pidm' => '123456',
                'spraddr_atyp_code' => 'LO',
                'spraddr_status_ind' => null,
                'spraddr_from_date' => '2016-01-12',
                'spraddr_to_date' => null,
                'spraddr_street_line1' => '1st street',
                'spraddr_street_line2' => 'APT 5',
                'spraddr_street_line3' => null,
                'spraddr_city' => 'Oxford',
                'spraddr_stat_code' => 'OH',
                'spraddr_zip' => '45056',
                'spraddr_natn_code' => '',
                'stvnatn_nation' => '',
                'spraddr_seqno' => '1',
                'row_id'=>'ABC',
                'spraddr_asrc_code' => null
            ],
            [
                'spraddr_pidm' => '123456',
                'spraddr_atyp_code' => 'MA',
                'spraddr_status_ind' => null,
                'spraddr_from_date' => '2016-01-12',
                'spraddr_to_date' => null,
                'spraddr_street_line1' => '3rd street',
                'spraddr_street_line2' => 'APT 12',
                'spraddr_street_line3' => null,
                'spraddr_city' => 'Oxford',
                'spraddr_stat_code' => 'OH',
                'spraddr_zip' => '45056',
                'spraddr_natn_code' => '',
                'stvnatn_nation' => '',
                'spraddr_seqno' => '1',
                'row_id'=>'ABC',
                'spraddr_asrc_code' => null
            ],
        ];

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $models = $this->address->filterType($types)->read($pidm);

        foreach ($types as $type) {
            $this->assertTrue(in_array($type, $this->queryParams));
        }
        $this->assertTrue(stripos($this->queryString, 'spraddr_atyp_code in') !== false, 'Query contains spraddr_atyp_code in condition');

    }

    public function testAddressReadByStatus()
    {

        $pidm = '123456';
        $status = ['active', 'future'];

        $this->records = [
            [
                'spraddr_pidm' => '123456',
                'spraddr_atyp_code' => 'LO',
                'spraddr_status_ind' => null,
                'spraddr_from_date' => '2016-01-12',
                'spraddr_to_date' => null,
                'spraddr_street_line1' => '1st street',
                'spraddr_street_line2' => 'APT 5',
                'spraddr_street_line3' => null,
                'spraddr_city' => 'Oxford',
                'spraddr_stat_code' => 'OH',
                'spraddr_zip' => '45056',
                'spraddr_natn_code' => '',
                'stvnatn_nation' => '',
                'spraddr_seqno' => '1',
                'row_id'=>'ABC',
                'spraddr_asrc_code' => null
            ],
            [
                'spraddr_pidm' => '123456',
                'spraddr_atyp_code' => 'LO',
                'spraddr_status_ind' => null,
                'spraddr_from_date' => '2017-08-12',
                'spraddr_to_date' => null,
                'spraddr_street_line1' => '1st street',
                'spraddr_street_line2' => 'APT 5',
                'spraddr_street_line3' => null,
                'spraddr_city' => 'Oxford',
                'spraddr_stat_code' => 'OH',
                'spraddr_zip' => '45056',
                'spraddr_natn_code' => '',
                'stvnatn_nation' => '',
                'spraddr_seqno' => '1',
                'row_id'=>'ABC',
                'spraddr_asrc_code' => null
            ],

        ];

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $models = $this->address->filterStatus($status)->read($pidm);


        $this->assertTrue(stripos($this->queryString, 'spraddr_status_ind is null') !== false, 'Query contains spraddr_status_ind in condition');
        $this->assertTrue(stripos($this->queryString, 'spraddr_from_date <= sysdate') !== false, 'Query contains spraddr_from_date <= sysdate in condition');
        $this->assertTrue(stripos($this->queryString, 'spraddr_from_date > sysdate') !== false, 'Query contains spraddr_from_date > sysdate in condition');
    }

    public function queryall_arrayWithQuery($subject)
    {
        $this->queryString = $subject;
        return true;
    }

    public function queryall_arrayWithParams($subject)
    {
        $this->queryParams = $subject;
        return true;
    }

    public function queryall_arrayMock()
    {
        return $this->records;
    }


}