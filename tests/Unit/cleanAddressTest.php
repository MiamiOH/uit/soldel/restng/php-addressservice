<?php

namespace MiamiOH\RestngAddressService\Tests\Unit;

class cleanAddressTest extends \MiamiOH\RESTng\Testing\TestCase
{
    private $cleanAddress;

    private $dbh;
    private $sth;
    private $queryString = '';
    private $bindPlaceHolder = '';
    private $boundValues = [];

    protected function setUp()
    {
        $this->queryString = '';
        $this->bindPlaceHolder = '';
        $this->boundValues = [];

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('prepare'))
            ->getMock();

        $this->sth = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\STH')
            ->setMethods(array('execute', 'bind_by_name'))
            ->getMock();

        $this->sth->method('bind_by_name')
            ->with($this->callback(array($this, 'bind_by_nameWithName')), $this->callback(array($this, 'bind_by_nameWithValue')))
            ->willReturn(true);

        //$this->dbh->method('prepare')->willReturn($this->sth);

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->cleanAddress = new \MiamiOH\RestngAddressService\Services\CleanAddress();

        $this->cleanAddress->setDatabase($db);

        $this->dbh->error_string = '';

    }

    public function testCleanAddressVerify()
    {
        $this->queryString = '';

        $model = [
            'pidm' => 123456,
            'addressType' => 'LO',
            'streetLine1' => '1st street',
            'streetLine2' => 'APT 5',
            'streetLine3' => null,
            'city' => 'Oxford',
            'state' => 'OH',
            'postalCode' => '45056',
            'sequenceNumber' => null,
            'status' => null,
        ];

        $this->dbh->expects($this->once())->method('prepare')
            ->with($this->callback(array($this, 'prepareWithQuery')))
            ->willReturn($this->sth);

        $this->cleanAddress->cleanAddressVerify($model);

        $this->assertTrue(strpos($this->queryString, 'CLEAN_ADDRESS.Verify') !== false, 'Query contain CLEAN_ADDRESS.Verify');
        $this->assertTrue(strpos($this->queryString, 'BEGIN') !== false, 'Query contain BEGIN');
        $this->assertTrue(strpos($this->queryString, 'AddressRec.Address_Line_1') !== false, 'Query contain AddressRec.Address_Line_1');

        $this->assertEquals($model['streetLine1'], $this->boundValues[':ADDRESSLINE1'], 'StreetLine1 is bound to correct value');
        $this->assertEquals($model['postalCode'], $this->boundValues[':POSTALCODE'], 'postalCode is bound to correct value');

    }

    public function prepareWithQuery($subject)
    {
        $this->queryString = $subject;
        return true;
    }

    public function bind_by_nameWithName($subject)
    {
        $this->bindPlaceHolder = $subject;
        return true;
    }

    public function bind_by_nameWithValue($subject)
    {
        $this->boundValues[$this->bindPlaceHolder] = $subject;
        $this->bindPlaceHolder = '';
        return true;
    }

}