<?php

namespace MiamiOH\RestngAddressService\Tests\Unit;

class updateAddressv2Test extends \MiamiOH\RESTng\Testing\TestCase
{
    private $updateAddress;

    private $dbh;

    private $records = [];
    private $queryString = '';
    private $queryParams = [];

    protected function setUp()
    {

        $this->records = [];
        $this->queryString = '';
        $this->queryParams = [];

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('perform'))
            ->getMock();

        $this->dbh->expects($this->once())->method('perform')
            ->with($this->callback(array($this, 'performWithQuery')),
                $this->callback(array($this, 'performWithParams')))
            ->will($this->returnCallback(array($this, 'performMock')));

        $this->updateAddress = new \MiamiOH\RestngAddressService\Services\UpdateAddressv2();
        $this->updateAddress->setDBH($this->dbh);

        $this->dbh->error_string = '';

    }

    public function testEndLocalAddress()
    {

        $this->records = [
            123456,
            'LO',
            123456,
            'LO',
        ];

        $this->updateAddress->endLocalAddress($pidms = "123456", $addressType = 'LO');

        $this->assertTrue(strpos($this->queryString, 'UPDATE') !== false, 'Query contain UPDATE spraddr');
        $this->assertTrue(strpos($this->queryString, 'SET') !== false, 'Query contain SET');
        $this->assertTrue(strpos($this->queryString, 'spraddr_status_ind') !== false, 'Query contain spraddr_status_ind');

    }

    public function performWithQuery($subject)
    {
        $this->queryString = $subject;
        return true;
    }

    public function performWithParams($subject)
    {
        $this->queryParams = $subject;
        return true;
    }

    public function performMock()
    {
        return $this->records;
    }

}