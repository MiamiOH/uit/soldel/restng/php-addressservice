# Test the basic api by querying api resources and services
Feature: Get an API response
  As a developer using the Address API
  I want to get information from the API
  In order to make sure it works

  Background:
    Given the test data is ready

  Scenario: Require authentication to get the address
    Given a REST client
    When I make a GET request for /person/address/v2/doej
    Then the HTTP status code is 401

  Scenario: Get the Address Information without authorization
    Given a REST client
    And a token for the TEST_WS_USER user
    When I make a GET request for /person/address/v2/doej
    Then the HTTP status code is 401

  Scenario: Get an Address Information with authentication and authorization with pidm as filter
    Given a REST client
    And a token for the ADDRESS_WS_USER user
    When I make a GET request for /person/address/v2/doej
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 1 entry
    And I get entry 1 from the subject
    And the subject has an element pidm equal to "123456"
    And the subject has an element addressType equal to "LO"
    And the subject has an element sequenceNumber equal to "2"
    And the subject has an element fromDate equal to "2006-08-13"
    And the subject has an element toDate equal to "2007-05-08"
    And the subject has an element streetLine1 equal to "114 E. Church St."
    And the subject has an element city equal to "Oxford"
    And the subject has an element state equal to "OH"
    And the subject has an element postalCode equal to "45056-1318"


  Scenario: Get an Address Information with authentication and authorization with Pidm and Addresstype as filter
    Given a REST client
    And a token for the ADDRESS_WS_USER user
    When I make a GET request for /person/address/v2/doej?addressType=LO
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 1 entry
    And I get entry 1 from the subject
    And the subject has an element pidm equal to "123456"
    And the subject has an element addressType equal to "LO"
    And the subject has an element sequenceNumber equal to "2"
    And the subject has an element fromDate equal to "2006-08-13"
    And the subject has an element toDate equal to "2007-05-08"
    And the subject has an element streetLine1 equal to "114 E. Church St."
    And the subject has an element city equal to "Oxford"
    And the subject has an element state equal to "OH"
    And the subject has an element postalCode equal to "45056-1318"

  Scenario: Request get address record without a pidm
    Given a REST client
    And a token for the ADDRESS_WS_USER user
    When I make a GET request for /person/address/v2
    Then the HTTP status code is 500


  Scenario: Request get address record with incorrect pidm
    Given a REST client
    And a token for the ADDRESS_WS_USER user
    When I make a GET request for /person/address/v2/smithd
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 0 entry

  Scenario: Get an Address Information with self authorization
    Given a REST client
    And a token for the DOEJ user
    When I make a GET request for /person/address/v2/doej
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 1 entry
    And I get entry 1 from the subject
    And the subject has an element pidm equal to "123456"
    And the subject has an element addressType equal to "LO"
    And the subject has an element sequenceNumber equal to "2"
    And the subject has an element fromDate equal to "2006-08-13"
    And the subject has an element toDate equal to "2007-05-08"
    And the subject has an element streetLine1 equal to "114 E. Church St."
    And the subject has an element city equal to "Oxford"
    And the subject has an element state equal to "OH"
    And the subject has an element postalCode equal to "45056-1318"

  Scenario: Get an Address Information with incorrect self authorization
    Given a REST client
    And a token for the DOEJ user
    When I make a GET request for /person/address/v2/smithd
    Then the HTTP status code is 401

  Scenario: Get an Address Information with authentication and authorization with Pidm,Addresstype and status as filter
    Given a REST client
    And a token for the ADDRESS_WS_USER user
    When I make a GET request for /person/address/v2/doej?addressType=LO&status=inactive
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 1 entry
    And I get entry 1 from the subject
    And the subject has an element pidm equal to "123456"
    And the subject has an element addressType equal to "LO"
    And the subject has an element status equal to "inactive"
    And the subject has an element sequenceNumber equal to "2"
    And the subject has an element fromDate equal to "2006-08-13"
    And the subject has an element toDate equal to "2007-05-08"
    And the subject has an element streetLine1 equal to "114 E. Church St."
    And the subject has an element city equal to "Oxford"
    And the subject has an element state equal to "OH"
    And the subject has an element postalCode equal to "45056-1318"

  Scenario: Get an Address Information with pidm and active status as filter
    Given a REST client
    And a token for the ADDRESS_WS_USER user
    When I make a GET request for /person/address/v2/howardt?status=active
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 2 entry

  Scenario: Get an Address Information with pidm and addressType as list
    Given a REST client
    And a token for the ADDRESS_WS_USER user
    When I make a GET request for /person/address/v2/souther?addressType=LO%2CRH
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 4 entry


  Scenario: Get an Address Information with pidm and status as list(active,future)
    Given a REST client
    And a token for the ADDRESS_WS_USER user
    When I make a GET request for /person/address/v2/howardt?status=active%2Cfuture
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 3 entry


