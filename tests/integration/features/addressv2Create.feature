Feature: Create address record of a user
  As a developer using the Address API
  I want to create the address record of a user
  In order to make sure it works

  Background:
    Given the test data is ready

  Scenario: Require authentication to Create an address resource
    Given a REST client
    When I make a POST request for /person/address/v2/jonesl?skipValidation=no
    Then the HTTP status code is 401

  Scenario: Create a new address record for a user
    Given a REST client
    And a token for the ADDRESS_WS_USER user
    And a collection of address records to create in the payload
    When I give the HTTP Content-type header with the value "application/json"
    And I make a POST request for /person/address/v2/jonesl?skipValidation=no
    Then the HTTP status code is 200
    And that an address record for jonesl with pidm "567890" does exist


  Scenario: Create a new MA address record for a user with existing previous active record(500 error)
    Given a REST client
    And a token for the ADDRESS_WS_USER user
    And a collection of MA address records to create in the payload
    When I give the HTTP Content-type header with the value "application/json"
    And I make a POST request for /person/address/v2/souther?skipValidation=no
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 1 entry
    And I get entry 1 from the subject
    And the subject has an element status equal to "500"

  Scenario: Create a new LO address record for a user with existing previous active record(200 NO error)
    Given a REST client
    And a token for the ADDRESS_WS_USER user
    And a collection of LO address records to create in the payload
    When I give the HTTP Content-type header with the value "application/json"
    And I make a POST request for /person/address/v2/souther?skipValidation=no
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 1 entry
    And I get entry 1 from the subject
    And the subject has an element status equal to "201"


  Scenario: Create a new LO address record for a user with existing previous active record with incorrect addressline1(400 error)
    Given a REST client
    And a token for the ADDRESS_WS_USER user
    And a collection of LO address records with incorrect addressline1 to create in the payload
    When I give the HTTP Content-type header with the value "application/json"
    And I make a POST request for /person/address/v2/souther?skipValidation=no
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 1 entry
    And I get entry 1 from the subject
    And the subject has an element status equal to "400"
    And the subject has an element errorCode equal to "M"
    And the subject has an element errorString equal to "Multiple Matches"
    And the subject has an element suggestionList equal to "123 XYZ Street;123 XYZ Blvd"

  Scenario: Create a new LO address record for a user with existing previous active record with incorrect zipcode(400 error)
    Given a REST client
    And a token for the ADDRESS_WS_USER user
    And a collection of LO address records with incorrect zipcode to create in the payload
    When I give the HTTP Content-type header with the value "application/json"
    And I make a POST request for /person/address/v2/souther?skipValidation=no
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 1 entry
    And I get entry 1 from the subject
    And the subject has an element status equal to "400"
    And the subject has an element errorCode equal to "Z"
    And the subject has an element errorString equal to "ZIP Code Error"

  Scenario: Create a new LO address record for a user with existing previous active record with incorrect state(400 error)
    Given a REST client
    And a token for the ADDRESS_WS_USER user
    And a collection of LO address records with incorrect state to create in the payload
    When I give the HTTP Content-type header with the value "application/json"
    And I make a POST request for /person/address/v2/souther?skipValidation=no
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 1 entry
    And I get entry 1 from the subject
    And the subject has an element status equal to "400"
    And the subject has an element errorCode equal to "R"
    And the subject has an element errorString equal to "Address out of Range"

  Scenario: Create a new MA address record with invalid address will not result in clean address verification failure
    Given a REST client
    And a token for the ADDRESS_WS_USER user
    And a collection of MA address records with invalid address to create in the payload
    When I give the HTTP Content-type header with the value "application/json"
    And I make a POST request for /person/address/v2/doej
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 1 entry
    And I get entry 1 from the subject
    And the subject has an element status equal to "201"

