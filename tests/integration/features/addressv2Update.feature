Feature: Create address record of a user
  As a developer using the Address API
  I want to end all the active previous
  records including future LO address
  in order to create new LO record.

  Background:
    Given the test data is ready

  Scenario: Update(using create API) a new LO address record for a user with existing two active address with one being future address
    Given a REST client
    And a token for the ADDRESS_WS_USER user
    And a collection of franklr LO records to create
    When I give the HTTP Content-type header with the value "application/json"
    And I make a POST request for /person/address/v2/franklr?skipValidation=no
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 1 entry
    And I get entry 1 from the subject
    And the subject has an element status equal to "201"
    And there is only one active address record for pidm "876543" does exist
