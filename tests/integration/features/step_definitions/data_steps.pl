#!perl

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use DBI;
use Data::Dumper;

use lib 'lib';
use StepConfig;

Given q/the test data is ready/, sub {

my $addressv2Data = loadDataFromFile('addressv2');
my $idData = loadDataFromFile('ids');

foreach my $table (qw(szbuniq SPRADDR)) {
    $bannerDbh->do(qq{
            delete from $table
        });
}

$bannerDbh->do(q{
        alter session set nls_date_format='DD-MON-YY'
    });

foreach my $record (@{$idData}) {
        $bannerDbh->do(q{
            insert into szbuniq (szbuniq_pidm, szbuniq_banner_id, szbuniq_unique_id, szbuniq_staff_ind, szbuniq_undergrad_ind)
                values (?, ?, ?, ?, ?)
            }, undef, $record->{'pidm'}, $record->{'banner_id'}, uc $record->{'uid'},
                uc $record->{'staff'}, uc $record->{'student'});
    }

foreach my $record (@{$addressv2Data}) {
    my $fromDate = $record->{'FROM_DATE'} =~ /sysdate/i ? $record->{'FROM_DATE'} : "'" . $record->{'FROM_DATE'} . "'";
    my $toDate = $record->{'TO_DATE'} =~ /sysdate/i ? $record->{'TO_DATE'} : "'" . $record->{'TO_DATE'} . "'";
    my $activityDate = $record->{'ACTIVITY_DATE'} =~ /sysdate/i ? $record->{'ACTIVITY_DATE'} : "'" . $record->{'ACTIVITY_DATE'} . "'";

    $bannerDbh->do(qq{
        insert into SPRADDR (SPRADDR_PIDM,SPRADDR_ATYP_CODE,SPRADDR_SEQNO,SPRADDR_FROM_DATE,SPRADDR_TO_DATE,SPRADDR_STREET_LINE1,
            SPRADDR_STREET_LINE2,SPRADDR_STREET_LINE3,SPRADDR_CITY,SPRADDR_STAT_CODE,SPRADDR_ZIP,SPRADDR_CNTY_CODE,SPRADDR_NATN_CODE,
            SPRADDR_PHONE_AREA,SPRADDR_PHONE_NUMBER,SPRADDR_PHONE_EXT,SPRADDR_STATUS_IND,SPRADDR_ACTIVITY_DATE,SPRADDR_USER)
            values (?, ?, ?, $fromDate, $toDate, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, $activityDate, ?)
        }, undef, $record->{'PIDM'}, $record->{'ATYP_CODE'}, $record->{'SEQNO'},
            $record->{'STREET_LINE1'}, $record->{'STREET_LINE2'}, $record->{'STREET_LINE3'}, $record->{'CITY'}, $record->{'STAT_CODE'},
            $record->{'ZIP'},$record->{'CNTY_CODE'},$record->{'NATN_CODE'},$record->{'PHONE_AREA'},$record->{'PHONE_NUMBER'},
            $record->{'PHONE_EXT'},$record->{'STATUS_IND'},$record->{'USER'});
}
};

sub loadDataFromFile {
    my $fileName = shift;

    my $data = [];

    open FILE, "../sql/sampleData/$fileName.txt" or die "Couldn't open ../sql/sampleData/$fileName.txt: $!";

    my $names = <FILE>;
    chomp $names;


    my @fieldNames = split("\t", $names);
    while (<FILE>) {
        chomp;
        my @values = split("\t");

        my $record = {};
        for (my $i = 0; $i < scalar(@fieldNames); $i++) {
            $record->{$fieldNames[$i]} = $values[$i];
        }
        push(@{$data}, $record);
    }

    close FILE;

    return $data;
}