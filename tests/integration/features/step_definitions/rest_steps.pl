#!perl

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use LWP::UserAgent;
use JSON;
use Data::Dumper;

use lib 'lib';
use StepConfig;

Given qr/a usable (\S+) class/, sub { use_ok( $1 ); };

Given q/a REST client/, sub {
    my $ua = LWP::UserAgent->new();
    ok( $ua, "UserAgent created" );
    S->{'ua'} = $ua;
    S->{'headers'} = [];
};

When qr/I give the HTTP ([^ ]+) header with the value "(\S+)"/, sub {
    push(@{S->{'headers'}}, {'name' => $1, 'value' => $2});
};

When qr/I make a (\S+) request for (\S+)/, sub {
    S->{'method'} = $1;
    S->{'path'} = $2;

    if (S->{'offset'} || S->{'limit'}) {
        if (S->{'path'} =~ /\?/) {
            S->{'path'} .= '&';
        } else {
            S->{'path'} .= '?';
        }

        S->{'path'} .= 'offset=' . S->{'offset'};
        S->{'path'} .= '&limit=' . S->{'limit'};
    }

    if (S->{'fields'}) {
        if (S->{'path'} =~ /\?/) {
            S->{'path'} .= '&';
        } else {
            S->{'path'} .= '?';
        }

        S->{'path'} .= 'fields=' . S->{'fields'};
    }

    my $path = S->{'path'};
    S->{'request'} = HTTP::Request->new(S->{'method'}, $server . $path);

    if (S->{'method'} eq 'POST') {
        # check content type for json or xml
        S->{'request'}->content(to_json(S->{'object'}));
    }

    foreach my $header (@{S->{'headers'}}) {
        S->{'request'}->header($header->{'name'}, $header->{'value'});
    }

    S->{'response'} = S->{'ua'}->simple_request(S->{'request'});
    #print S->{'request'}->as_string();
    #print S->{'response'}->as_string();

};

When qr/I dump the (response|request)/, sub {
    print S->{$1}->as_string();
};

When qr/I dump the (body)/, sub {
    my $body = from_json(${S->{'response'}->content_ref()});
    print Dumper($body);
};

Then qr/the HTTP status code is (.+)/, sub {
    is( S->{'response'}->code(), $1 );
};

Then qr/the HTTP (\S+) header is "(\S+)"/, sub {
    is( S->{'response'}->header($1), $2 );
};

Then qr/the response (\S+) element does not contain a (\S+) key/, sub {
    my $body = from_json(${S->{'response'}->content_ref()});

    my $element = $1;
    my $key = $2;

    ok(defined($body->{$element}), "Body has element $element");
    ok(!defined($body->{$element}{$key}), "Element $element in body does not have a $key");
};

Then qr/the response contains a (\S+) element matching "([^"]+)"/, sub {
    my $body = from_json(${S->{'response'}->content_ref()});

    my $element = $1;
    my $value = $2;

    ok(defined($body->{$element}), "Body has element $element");
    ok($body->{$element} =~ /$value/, "Element $element in body matches $value");
};

Then qr/the response (\S+) element ([^ ]+) entry contains a (\S+) key (matching|equal to|not equal to) "([^"]+)"/, sub {
    my $body = from_json(${S->{'response'}->content_ref()});

    my $element = $1;
    my $entry = $2;
    my $key = $3;
    my $comparison = $4;
    my $value = $5;

    my $index = 0;

    $index = 0 if ($entry eq 'first');
    $index = 1 if ($entry eq 'second');
    $index = 2 if ($entry eq 'third');
    $index = 3 if ($entry eq 'fourth');
    $index = $#{$body->{$element}} if ($entry eq 'last');

    ok(defined($body->{$element}), "Element $element in body");
    ok(defined($body->{$element}[$index]{$key}), "Element $element in body has key $key");
    
    if (JSON::is_bool($body->{$element}[$index]{$key})) {
        $body->{$element}[$index]{$key} = $body->{$element}[$index]{$key} ? 'true' : 'false';
    }

    if ($comparison eq 'matching') {
        ok($body->{$element}[$index]{$key} =~ /$value/, "Element $element $entry entry in body has key $key $comparison $value");
    } elsif ($comparison eq 'not equal to') {
        ok($body->{$element}[$index]{$key} ne $value, "Element $element $entry entry in body has key $key $comparison $value");
    } else {
        ok($body->{$element}[$index]{$key} eq $value, "Element $element $entry entry in body has key $key $comparison $value");
    }
};

Then qr/the response (\S+) element contains a (\S+) key (matching|equal to|not equal to) "([^"]+)"/, sub {
    my $body = from_json(${S->{'response'}->content_ref()});

    my $element = $1;
    my $key = $2;
    my $comparison = $3;
    my $value = $4;

    ok(defined($body->{$element}), "Element $element in body");
    ok(defined($body->{$element}{$key}), "Element $element in body has key $key");
    
    if (JSON::is_bool($body->{$element}{$key})) {
        $body->{$element}{$key} = $body->{$element}{$key} ? 'true' : 'false';
    }

    if ($comparison eq 'matching') {
        ok($body->{$element}{$key} =~ /$value/, "Element $element in body has key $key $comparison $value");
    } elsif ($comparison eq 'not equal to') {
        ok($body->{$element}{$key} ne $value, "Element $element in body has key $key $comparison $value");
    } else {
        ok($body->{$element}{$key} eq $value, "Element $element in body has key $key $comparison $value");
    }
};

Then qr/the response (\S+) element contains a (\S+) key which is an (\S+) matching "([^"]+)"/, sub {
    my $body = from_json(${S->{'response'}->content_ref()});

    my $element = $1;
    my $key = $2;
    my $type = lc $3;
    my $value = $4;

    my $responseType = ref($body->{$element}{$key}) ? ref($body->{$element}{$key}) : 'SCALAR';

    my $responseValue = '';
    if ($type eq 'array' && $responseType eq 'ARRAY') {
        $responseValue = join(',', @{$body->{$element}{$key}});
    } elsif ($type eq 'scalar' && $responseType eq 'SCALAR') {
        $responseValue = $body->{$element}{$key};
    } else {
        $responseValue = '';
    }

    ok(defined($body->{$element}), "Element $element in body");
    ok(defined($body->{$element}{$key}), "Element $element in body has key $key");
    ok(lc $responseType eq $type, "Element $element key $key is type $type");
    ok($responseValue eq $value, "Element $element in body has key $key matches $value");
};

Then qr/the response (\S+) element contains a (\S+) key which is an? (\S+)/, sub {
    my $body = from_json(${S->{'response'}->content_ref()});

    my $element = $1;
    my $key = $2;
    my $type = lc $3;
    my $value = $4;

    my $responseType = ref($body->{$element}{$key}) ? ref($body->{$element}{$key}) : 'SCALAR';

    ok(defined($body->{$element}), "Element $element in body");
    ok(defined($body->{$element}{$key}), "Element $element in body has key $key");
    ok(lc $responseType eq $type, "Element $element key $key is type $type");
};

Then qr/an address record for (\S+) with pidm "(\S+)" (.*)/, sub {
	my $uid = $1;
	my $value = $2;
	my $spec = $3;
	my $status = $3 eq 'does exist' ? 1 : 0;

	my($realStatus) = $bannerDbh->selectrow_array(q{
		select count(*)
			from saturn.spraddr inner join szbuniq
			    on szbuniq_pidm = saturn.spraddr.spraddr_pidm
			where szbuniq_unique_id = upper(?)
			    and spraddr_pidm = ?
		}, undef, $uid, $value);

	ok($realStatus == $status, "An address record for $uid with pidm $value $spec");

};

Then qr/there is only one active address record for pidm "(\S+)" (.*)/, sub {
	my $pidm = $1;
	my $spec = $2;
	my $status = $2 eq 'does exist' ? 1 : 0;
	my($realStatus) = $bannerDbh->selectrow_array(q{
		select count(*)
			from saturn.spraddr
			where spraddr_pidm = ? and spraddr_atyp_code = 'LO' and spraddr_status_ind is null
		}, undef, $pidm);

	ok($realStatus == $status, "An address record for $pidm with pidm $spec");

};

