#!perl

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use LWP::UserAgent;
use JSON;
use Data::Dumper;

use lib 'lib';
use StepConfig;

Given qr/a collection of address records to create in the payload/, sub {
S->{'objectType'} = 'collection';
S->{'object'} = [
            {
                 'pidm' => '567890',
                 'addressType' => 'LO',
                 'streetLine1'=> '5188 red cloud',
                 'streetLine2'=> 'apt 5',
                 'city'=> 'oxford',
                 'state'=> 'OH',
                 'postalCode'=> '45056',
                 'sequenceNumber'=> '1',
                 'status'=>'',
             },
        ];

};

Given qr/a collection of MA address records to create in the payload/, sub {
S->{'objectType'} = 'collection';
S->{'object'} = [
            {
                 'pidm' => '876253',
                 'addressType' => 'MA',
                 'streetLine1'=> '3rd street',
                 'streetLine2'=> 'apt 10',
                 'city'=> 'oxford',
                 'state'=> 'OH',
                 'postalCode'=> '45056',
                 'sequenceNumber'=> '1',
                 'status'=>'',
             },
        ];

};

Given qr/a collection of LO address records to create in the payload/, sub {
S->{'objectType'} = 'collection';
S->{'object'} = [
            {
                 'pidm' => '876253',
                 'addressType' => 'LO',
                 'streetLine1'=> '4th street',
                 'streetLine2'=> 'apt 15',
                 'city'=> 'oxford',
                 'state'=> 'OH',
                 'postalCode'=> '45056',
                 'sequenceNumber'=> '1',
                 'status'=>'',
             },
        ];

};

Given qr/a collection of LO address records with incorrect zipcode to create in the payload/, sub {
S->{'objectType'} = 'collection';
S->{'object'} = [
            {
                 'pidm' => '876253',
                 'addressType' => 'LO',
                 'streetLine1'=> '4th Street',
                 'streetLine2'=> 'apt 15',
                 'city'=> 'oxford',
                 'state'=> 'OH',
                 'postalCode'=> '00000',
                 'sequenceNumber'=> '1',
                 'status'=>'',
             },
        ];

};

Given qr/a collection of LO address records with incorrect state to create in the payload/, sub {
S->{'objectType'} = 'collection';
S->{'object'} = [
            {
                 'pidm' => '876253',
                 'addressType' => 'LO',
                 'streetLine1'=> '4th Street',
                 'streetLine2'=> 'apt 15',
                 'city'=> 'oxford',
                 'state'=> 'ABCD',
                 'postalCode'=> '45056',
                 'sequenceNumber'=> '1',
                 'status'=>'',
             },
        ];

};

Given qr/a collection of LO address records with incorrect addressline1 to create in the payload/, sub {
S->{'objectType'} = 'collection';
S->{'object'} = [
            {
                 'pidm' => '876253',
                 'addressType' => 'LO',
                 'streetLine1'=> '123 XYZ',
                 'streetLine2'=> 'apt 15',
                 'city'=> 'oxford',
                 'state'=> 'OH',
                 'postalCode'=> '45056',
                 'sequenceNumber'=> '1',
                 'status'=>'',
             },
        ];

};

Given qr/a collection of franklr LO records to create/, sub {
 S->{'objectType'} = 'collection';
 S->{'object'} = [
             {
                  'pidm' => '876543',
                  'addressType' => 'LO',
                  'streetLine1'=> '4th street',
                  'streetLine2'=> 'apt 15',
                  'city'=> 'oxford',
                  'state'=> 'OH',
                  'postalCode'=> '45056',
                  'sequenceNumber'=> '5',
                  'status'=> '',
              },
         ];

 };

 Given qr/a collection of MA address records with invalid address to create in the payload/, sub {
 S->{'objectType'} = 'collection';
 S->{'object'} = [
             {
                  'pidm' => '123456',
                  'addressType' => 'MA',
                  'streetLine1'=> '123 XYZ',
                  'streetLine2'=> 'apt 10',
                  'city'=> 'oxford',
                  'state'=> 'OH',
                  'postalCode'=> '45056',
                  'sequenceNumber'=> '1',
                  'status'=>'',
              },
         ];

 };
